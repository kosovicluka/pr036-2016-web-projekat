﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web1App.ViewModelComponents
{
    public enum AccommodationSortOption
    {
        NAME_A,
        NAME_D,
        UNITS_A,
        UNITS_D,
        FREE_UNITS_A,
        FREE_UNITS_D
    }

    public class AccommodationSort
    {
        public AccommodationSort()
        {

        }

        public AccommodationSort(AccommodationSortOption option)
        {
            Option = option;
        }

        public AccommodationSortOption Option { get; set; }

        public List<SelectListItem> BindAccommodationSort()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "Name (ascending)",
                Value = AccommodationSortOption.NAME_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Name (descending)",
                Value = AccommodationSortOption.NAME_D.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Total number of units (ascending)",
                Value = AccommodationSortOption.UNITS_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Total number of units (descending)",
                Value = AccommodationSortOption.UNITS_D.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Number of free units (ascending)",
                Value = AccommodationSortOption.FREE_UNITS_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Number of free units (descending)",
                Value = AccommodationSortOption.FREE_UNITS_D.ToString()
            });
            SelectListItem item = items.Find(x => x.Value == Option.ToString());
            item.Selected = true;

            return items;
        }
    }
}