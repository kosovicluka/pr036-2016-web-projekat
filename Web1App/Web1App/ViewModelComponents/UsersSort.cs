﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web1App.ViewModelComponents
{
    public enum UsersSortOption
    {
        NAME_A,
        NAME_D,
        SURNAME_A,
        SURNAME_D,
        ROLE_A,
        ROLE_D
    }
    public class UsersSort
    {
        public UsersSort()
        {

        }

        public UsersSort(UsersSortOption option)
        {
            Option = option;
        }

        public UsersSortOption Option { get; set; } = UsersSortOption.NAME_A;

        public List<SelectListItem> BindUsersSort()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "Name (ascending)",
                Value = UsersSortOption.NAME_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Name (descending)",
                Value = UsersSortOption.NAME_D.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Surname (ascending)",
                Value = UsersSortOption.SURNAME_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Surname (descending)",
                Value = UsersSortOption.SURNAME_D.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Role (ascending)",
                Value = UsersSortOption.ROLE_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Role (descending)",
                Value = UsersSortOption.ROLE_D.ToString()
            });
            SelectListItem item = items.Find(x => x.Value == Option.ToString());
            item.Selected = true;

            return items;
        }
    }
}