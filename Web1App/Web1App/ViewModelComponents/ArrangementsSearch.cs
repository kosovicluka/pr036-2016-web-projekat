﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web1App.Models;

namespace Web1App.ViewModelComponents
{
    public class ArrangementsSearch
    {
        public string StartDateFrom { get; set; }
        public string StartDateTo { get; set; }
        public string EndDateFrom { get; set; }
        public string EndDateTo { get; set; }
        public string Name { get; set; }
        public Transport Transport { get; set; }
        public ArrangementType ArrangementType { get; set; }

        public ArrangementsSearch()
        {

        }

        public ArrangementsSearch(
            ArrangementType arrangementType,
            Transport transport,
            string startDateFrom,
            string startDateTo,
            string endDateFrom,
            string endDateTo,
            string name)
        {
            ArrangementType = arrangementType;
            Transport = transport;
            StartDateFrom = startDateFrom;
            StartDateTo = startDateTo;
            EndDateFrom = endDateFrom;
            EndDateTo = endDateTo;
            Name = name;
        }

        public List<SelectListItem> BindTransport()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "Bus",
                Value = Transport.BUS.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Airplane",
                Value = Transport.AIRPLANE.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Bus&Airplane",
                Value = Transport.BUSNAIRPLANE.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Individual",
                Value = Transport.INDIVIDUAL.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Other",
                Value = Transport.OTHER.ToString()
            });
            SelectListItem item = items.Find(x => x.Value == this.Transport.ToString());
            item.Selected = true;

            return items;
        }
        public List<SelectListItem> BindArrangementType()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "BB",
                Value = ArrangementType.BB.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Half Board",
                Value = ArrangementType.HALFBOARD.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Full Board",
                Value = ArrangementType.FULLBOARD.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "All Inclusive",
                Value = ArrangementType.ALLINCLUSIVE.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Rent Apartment",
                Value = ArrangementType.RENTAPARTMENT.ToString()
            });
            SelectListItem item = items.Find(x => x.Value == this.ArrangementType.ToString());
            item.Selected = true;

            return items;
        }
    }
}