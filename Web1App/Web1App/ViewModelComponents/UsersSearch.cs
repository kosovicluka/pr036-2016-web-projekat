﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web1App.Models;

namespace Web1App.ViewModelComponents
{
    public class UsersSearch
    {
        public UsersSearch()
        {

        }

        public UsersSearch(string name, string surname, Role role)
        {
            Name = name;
            Surname = surname;
            Role = role;
        }

        public string Name { get; set; }

        public string Surname { get; set; }

        public Role Role { get; set; } = Role.ADMINISTRATOR;

        public List<SelectListItem> BindRole()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "ADMINISTRATOR",
                Value = Role.ADMINISTRATOR.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "MANAGER",
                Value = Role.MANAGER.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "TOURIST",
                Value = Role.TOURIST.ToString()
            });
            SelectListItem item = items.Find(x => x.Value == Role.ToString());
            item.Selected = true;

            return items;
        }
    }
}