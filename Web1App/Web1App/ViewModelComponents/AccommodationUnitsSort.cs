﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web1App.ViewModelComponents
{
    public enum AccommodationUnitsSortOption
    {
        ANOG_A, // Allowed number of guests (ascending)
        ANOG_D,
        PRICE_A,
        PRICE_D
    }
    public class AccommodationUnitsSort
    {
        public AccommodationUnitsSort()
        {

        }
        public AccommodationUnitsSort(AccommodationUnitsSortOption option)
        {
            this.Option = option;
        }
        public AccommodationUnitsSortOption Option { get; set; } = AccommodationUnitsSortOption.ANOG_A;
        public List<SelectListItem> BindAccommodationUnitsSort()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "Allowed number of guests (ascending)",
                Value = AccommodationUnitsSortOption.ANOG_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Allowed number of guests (descending)",
                Value = AccommodationUnitsSortOption.ANOG_D.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Price (ascending)",
                Value = AccommodationUnitsSortOption.PRICE_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Price (descending)",
                Value = AccommodationUnitsSortOption.PRICE_D.ToString()
            });
            SelectListItem item = items.Find(x => x.Value == this.Option.ToString());
            item.Selected = true;

            return items;
        }
    }
}