﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web1App.Models;

namespace Web1App.ViewModelComponents
{
    public class ReservationSearch
    {
        public ReservationSearch()
        {

        }

        public string TouristUsername { get; set; }
        public ReservationStatus Status { get; set; }
        public string ArrangementName { get; set; }
        public string AccommodationName { get; set; }
        public string AccommodationUnitName { get; set; }

        public List<SelectListItem> BindStatus()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "Active",
                Value = ReservationStatus.ACTIVE.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Inactive",
                Value = ReservationStatus.INACTIVE.ToString()
            });
            
            SelectListItem item = items.Find(x => x.Value == Status.ToString());
            item.Selected = true;

            return items;
        }
    }
}