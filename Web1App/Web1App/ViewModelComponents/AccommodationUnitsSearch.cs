﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web1App.ViewModelComponents
{
    public class AccommodationUnitsSearch
    {
        public AccommodationUnitsSearch()
        {

        }

        public int MinNumberOfGuests { get; set; }
        public int MaxNumberOfGuests { get; set; }
        public bool PetFree { get; set; } = false;
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
    }
}