﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web1App.ViewModelComponents
{
    public enum ReservationSortOption
    {
        TOURIST_A,
        TOURIST_D,
        STATUS_A,
        STATUS_D,
        ARRANGEMENT_A,
        ARRANGEMENT_D,
        ACCOMMODATION_A,
        ACCOMMODATION_D,
        ACCOMMODATION_UNIT_A,
        ACCOMMODATION_UNIT_D,
    }

    public class ReservationSort
    {
        public ReservationSort()
        {

        }

        public ReservationSort(ReservationSortOption option)
        {
            Option = option;
        }

        public ReservationSortOption Option { get; set; }

        public List<SelectListItem> BindReservationSort()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "Tourist Username (ascending)",
                Value = ReservationSortOption.TOURIST_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Tourist Username (descending)",
                Value = ReservationSortOption.TOURIST_D.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Reservation Status (ascending)",
                Value = ReservationSortOption.STATUS_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Reservation Status (descending)",
                Value = ReservationSortOption.STATUS_D.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Arrangement Name (ascending)",
                Value = ReservationSortOption.ARRANGEMENT_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Arrangement Name (descending)",
                Value = ReservationSortOption.ARRANGEMENT_D.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Accommodation Name (ascending)",
                Value = ReservationSortOption.ACCOMMODATION_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Accommodation Name (descending)",
                Value = ReservationSortOption.ACCOMMODATION_D.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Accommodation Unit Name (ascending)",
                Value = ReservationSortOption.ACCOMMODATION_UNIT_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Accommodation Unit Name (descending)",
                Value = ReservationSortOption.ACCOMMODATION_UNIT_D.ToString()
            });

            SelectListItem item = items.Find(x => x.Value == Option.ToString());
            item.Selected = true;

            return items;
        }
    }
}