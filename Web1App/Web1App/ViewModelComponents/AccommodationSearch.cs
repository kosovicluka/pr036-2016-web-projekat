﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web1App.Models;

namespace Web1App.ViewModelComponents
{
    public class AccommodationSearch
    {
        public AccommodationSearch()
        {

        }

        public AccommodationSearch(
            AccommodationType type,
            string name,
            bool pool,
            bool spa,
            bool adaptedForPwds,
            bool wiFi
            )
        {
            AccommodationType = type;
            Name = name;
            Pool = pool;
            Spa = spa;
            AdaptedForPWDs = adaptedForPwds;
            WiFi = wiFi;
        }

        public AccommodationType AccommodationType { get; set; }
        public string Name { get; set; }
        public bool Pool { get; set; }
        public bool Spa { get; set; }
        public bool AdaptedForPWDs { get; set; }
        public bool WiFi { get; set; }

        public List<SelectListItem> BindAccommodationType()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "Hotel",
                Value = AccommodationType.HOTEL.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Motel",
                Value = AccommodationType.MOTEL.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Villa",
                Value = AccommodationType.VILLA.ToString()
            });
            SelectListItem item = items.Find(x => x.Value == this.AccommodationType.ToString());
            item.Selected = true;

            return items;
        }

    }
}