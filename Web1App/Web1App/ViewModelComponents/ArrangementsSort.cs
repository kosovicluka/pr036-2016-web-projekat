﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web1App.ViewModelComponents
{
    public enum ArrangementsSortOption
    {
        NAME_A,
        NAME_D,
        STARTDATE_A,
        STARTDATE_D,
        ENDDATE_A,
        ENDDATE_D
    }
    public class ArrangementsSort
    {

        public ArrangementsSort() { }

        public ArrangementsSortOption Option { get; set; }

        public List<SelectListItem> BindArrangementSort()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "Name (ascending)",
                Value = ArrangementsSortOption.NAME_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Name (descending)",
                Value = ArrangementsSortOption.NAME_D.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Start date (ascending)",
                Value = ArrangementsSortOption.STARTDATE_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Start date (descending)",
                Value = ArrangementsSortOption.STARTDATE_D.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "End date (ascending)",
                Value = ArrangementsSortOption.ENDDATE_A.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "End date (descending)",
                Value = ArrangementsSortOption.ENDDATE_D.ToString()
            });
            SelectListItem item = items.Find(x => x.Value == Option.ToString());
            item.Selected = true;

            return items;
        }
    }
}