﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web1App.Models
{
    public class AccommodationUnit
    {
        public AccommodationUnit()
        {
            ID = Guid.NewGuid().ToString();
        }
        public AccommodationUnit(string name, int maxNumOfGuests, bool pets, double price)
        {
            MaxNumOfGuests = maxNumOfGuests;
            Pets = pets;
            Price = price;
            Active = true;
            Name = name;
            ID = Guid.NewGuid().ToString();
        }

        [Required, RegularExpression("/^[A-Za-z]+$/")]
        public string Name { get; set; }
        [Required, Range(0, Int32.MaxValue)]
        public int MaxNumOfGuests { get; set; }
        [Required]
        public bool Pets { get; set; }
        [Required, Range(0, Double.MaxValue)]
        public double Price { get; set; }
        public bool Active { get; set; } = true;
        public string ID { get; set; }
        public bool Free { get; set; } = true;
    }
}