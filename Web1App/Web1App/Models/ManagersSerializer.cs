﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Web1App.Models
{
    public class ManagersSerializer
    {
        private static readonly XmlSerializer serializer =
            new XmlSerializer(typeof(List<Manager>), new XmlRootAttribute("Managers"));
        private static readonly string path = System.AppContext.BaseDirectory + @"Xmls\Managers.xml";
        private static readonly object lockObj = new object();

        #region Serialization/Deserialization
        private static void Serialize(List<Manager> managers)
        {
            using (FileStream fs = new FileStream(path, FileMode.Truncate, FileAccess.Write, FileShare.None))
            {
                serializer.Serialize(fs, managers);
                fs.Close();
            }
        }

        private static List<Manager> Deserialize()
        {
            List<Manager> managers = new List<Manager>(); ;

            if (File.Exists(path))
            {
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);

                // empty xml
                if (fs.Length == 0)
                {
                    fs.Close();
                    return managers;
                }

                //ucitavanje aranzmana
                managers = (List<Manager>)serializer.Deserialize(fs);
                if (managers == null)
                    managers = new List<Manager>();
                else
                    // samo aktivni menadzeri ce biti ucitani u aplikaciju
                    managers = managers.FindAll(x => x.Active);

                fs.Close();
            }

            return managers;
        }
        #endregion

        public static void Create(Manager a)
        {
            lock (lockObj)
            {
                List<Manager> managers = Deserialize();
                managers.Add(a);
                Serialize(managers);
            }
        }

        public static Manager Read(string username)
        {
            Manager read = null;
            lock (lockObj)
            {
                read = Deserialize().Find(x => x.Username == username);
            }
            return read;
        }
        public static List<Manager> Read()
        {
            List<Manager> managers = null;
            lock (lockObj)
            {
                managers = Deserialize();
            }
            return managers;
        }

        public static bool Update(Manager a, string oldUsername)
        {
            bool updateSuccess = true;
            lock (lockObj)
            {
                List<Manager> managers = Deserialize();
                Manager old = managers.Find(x => x.Username == oldUsername);
                if (old == null)
                    updateSuccess = false;
                else
                {
                    managers.Remove(old);
                    managers.Add(a);
                    Serialize(managers);
                }
            }
            return updateSuccess;
        }

        public static bool Delete(string username)
        {
            bool deleted = true;
            lock (lockObj)
            {
                List<Manager> managers = Deserialize();
                Manager a = managers.Find(x => x.Username == username);
                if (a == null)
                    deleted = false;
                else
                {
                    managers.Remove(a);
                    Serialize(managers);
                }
            }
            return deleted;
        }
    }
}