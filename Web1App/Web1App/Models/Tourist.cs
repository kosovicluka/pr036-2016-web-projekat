﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web1App.Models
{
    public class Tourist : User
    {
        public Tourist()
        {

        }

        public Tourist(string username, string password,
            string name, string surname, string email, string date,
            Gender gender) : base(username, password,
                name, surname, email, date, gender, Role.TOURIST)
        {
            
        }

        public bool Blocked { get; set; } = false;
        public bool Active { get; set; } = true;
        public int NumOfCancelings { get; set; } = 0;
        public List<string> ReservationsIDs { get; set; }

        #region Reservations
        private List<Reservation> Reservations { get; set; } = new List<Reservation>();
        public void SetReservations(List<Reservation> reservations)
        {
            this.Reservations = reservations;
            ReservationsIDs = new List<string>();
            foreach (var r in this.Reservations)
            {
                ReservationsIDs.Add(r.ID);
            }
        }
        public List<Reservation> GetReservations()
        {
            return this.Reservations;
        }
        public void AddReservation(Reservation r)
        {
            this.Reservations.Add(r);
            if (!this.ReservationsIDs.Contains(r.ID))
            {
                this.ReservationsIDs.Add(r.ID);
            }
        }

        #endregion
    }
}