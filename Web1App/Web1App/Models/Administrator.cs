﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web1App.Models
{
    public class Administrator : User
    {
        public Administrator()
        {

        }

        public Administrator(string username, string password,
            string name, string surname, string email, string date,
            Gender gender) : base(username, password,
                name, surname, email, date, gender, Role.ADMINISTRATOR)
        {

        }
    }
}