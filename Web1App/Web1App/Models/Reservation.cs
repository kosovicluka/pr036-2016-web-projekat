﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web1App.Models
{
    public enum ReservationStatus
    {
        ACTIVE,
        INACTIVE
    }

    public class Reservation
    {
        public Reservation()
        {
            ID = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, 16);
        }

        public Reservation(string tourist,
            ReservationStatus reservationStatus,
            string arrangement,
            string accommodation,
            string accommodationUnit)
        {
            ID = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, 16);
            TouristID = tourist;
            _ReservationStatus = reservationStatus;
            ArrangementID = arrangement;
            AccommodationID = accommodation;
            AccommodationUnitID = accommodationUnit;
        }

        public string ID { get; set; }
        public string TouristID { get; set; }
        public string ArrangementID { get; set; }
        public string AccommodationID { get; set; }
        public string AccommodationUnitID { get; set; }
        public ReservationStatus _ReservationStatus { get; set; }
        public bool Active { get; set; } = true;

        private Tourist Tourist { get; set; }
        public void SetTourist(Tourist t)
        {
            Tourist = t;
        }
        public Tourist GetTourist()
        {
            return Tourist;
        }
        private Arrangement Arrangement { get; set; }
        public void SetArrangement(Arrangement a)
        {
            Arrangement = a;
        }
        public Arrangement GetArrangement()
        {
            return Arrangement;
        }
        private Accommodation Accommodation { get; set; }
        public void SetAccommodation(Accommodation a)
        {
            Accommodation = a;
        }
        public Accommodation GetAccommodation()
        {
            return Accommodation;
        }
        private AccommodationUnit AccommodationUnit { get; set; }
        public void SetAccommodationUnit(AccommodationUnit a)
        {
            AccommodationUnit = a;
        }
        public AccommodationUnit GetAccommodationUnit()
        {
            return AccommodationUnit;
        }
    }
}