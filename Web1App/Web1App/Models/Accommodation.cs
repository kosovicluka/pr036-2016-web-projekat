﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web1App.Models
{
    public enum AccommodationType
    {
        HOTEL,
        MOTEL,
        VILLA
    }

    public class Accommodation
    {
        public List<AccommodationUnit> Units { get; set; }
        public Accommodation()
        {
            ID = Guid.NewGuid().ToString();
            Units = new List<AccommodationUnit>();
            NumberOfFreeUnits = 0;
            TotalNumberOfUnits = 0;
        }
        public Accommodation(AccommodationType accommodationType, string name, bool pool, bool spa, bool adaptedForDisabledPeople, bool wifi)
        {
            _AccommodationType = accommodationType;
            Name = name;
            Pool = pool;
            Spa = spa;
            AdaptedForDisabledPeople = adaptedForDisabledPeople;
            WiFi = wifi;
            Active = true;
            ID = Guid.NewGuid().ToString();
            Units = new List<AccommodationUnit>();
            NumberOfFreeUnits = 0;
            TotalNumberOfUnits = 0;
        }

        [Required]
        public AccommodationType _AccommodationType { get; set; }
        [Required, RegularExpression("/^[A-Za-z]+$/")]
        public string Name { get; set; }
        [Required]
        public bool Pool { get; set; }
        [Required]
        public bool Spa { get; set; }
        [Required]
        public bool AdaptedForDisabledPeople { get; set; }
        [Required]
        public bool WiFi { get; set; }
        public bool Active { get; set; }
        public string ID { get; set; }
        public int NumberOfFreeUnits { get; set; }
        public int TotalNumberOfUnits { get; set; }

        public List<SelectListItem> BindAccommodationType()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "HOTEL",
                Value = AccommodationType.HOTEL.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "MOTEL",
                Value = AccommodationType.MOTEL.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "VILLA",
                Value = AccommodationType.VILLA.ToString()
            });
            SelectListItem item = items.Find(x => x.Value == this._AccommodationType.ToString());
            item.Selected = true;

            return items;
        }

        public void SetUnits(List<AccommodationUnit> units)
        {
            if (units == null)
                return;
            this.Units = units;
            TotalNumberOfUnits = units.Count;
            CalculateNumberOfFreeUnits();
            
        }

        public List<AccommodationUnit> GetActiveUnits()
        {
            return this.Units.FindAll(x => x.Active);
        }

        public List<AccommodationUnit> GetInactiveUnits()
        {
            return this.Units.FindAll(x => !x.Active);
        }

        public void AddUnit(AccommodationUnit a)
        {
            this.Units.Add(a);
            TotalNumberOfUnits++;
            NumberOfFreeUnits++;
        }

        public void RemoveUnit(AccommodationUnit a)
        {
            a.Active = false;
            TotalNumberOfUnits--;
            NumberOfFreeUnits--;
        }

        public void CalculateNumberOfFreeUnits()
        {
            if ((NumberOfFreeUnits = this.GetActiveUnits().Count) > 0)
            {
                NumberOfFreeUnits = this.GetActiveUnits().FindAll(x => x.Free).Count;
            }
        }
    }
}