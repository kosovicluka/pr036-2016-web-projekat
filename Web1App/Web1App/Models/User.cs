﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web1App.Models
{
    public enum Gender
    {
        MALE = 0,
        FEMALE,
        OTHER
    }

    public enum Role
    {
        ADMINISTRATOR,
        MANAGER,
        TOURIST
    }

    public class User
    {
        public User()
        {

        }

        public User(string username, string password, string name, string surname,
            string email, string date, Gender gender, Role role)
        {
            Username = username;
            Password = password;
            Name = name;
            Surname = surname;
            Email = email;
            Date = date;
            _Gender = gender;
            _Role = role;
        }

        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Date { get; set; } // dd/MM/yyyy
        [Required]
        public Gender _Gender { get; set; }
        [Required]
        public Role _Role { get; set; }
    }
}