﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web1App.Models
{
    public class PlaceOfDeparture
    {
        public PlaceOfDeparture()
        {

        }

        public PlaceOfDeparture(string address, double latitude, double longitude)
        {
            Address = address;
            Latitude = latitude;
            Longitude = longitude;
        }

        public string Address { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public string GetStreet()
        {
            return this.Address.Split('|')[0];
        }

        public string GetHouseNumber()
        {
            return this.Address.Split('|')[1];
        }

        public string GetCity()
        {
            return this.Address.Split('|')[2];
        }

        public int GetPostalCode()
        {
            return Convert.ToInt32(this.Address.Split('|')[3]);
        }
    }
}