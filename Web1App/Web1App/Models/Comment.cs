﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web1App.Models
{
    public enum CommentStatus
    {
        PENDING = 0,
        APPROVED,
        DISAPPROVED
    }
    public class Comment
    {
        public Comment()
        {
            ID = Guid.NewGuid().ToString();
        }

        public Comment(Tourist tourist, Arrangement arrangement, string text, short rate)
        {
            _Tourist = tourist;
            _Arrangement = arrangement;
            Text = text;
            Rate = rate;
            _CommentStatus = CommentStatus.PENDING;
            ID = Guid.NewGuid().ToString();
        }
        public string ID { get; set; }
        private Tourist _Tourist { get; set; }
        private Arrangement _Arrangement { get; set; }

        public Tourist GetTourist()
        {
            return this._Tourist;
        }

        public void SetTourist(Tourist t)
        {
            this._Tourist = t;
            this.TouristID = t.Username;
        }

        public Arrangement GetArrangement()
        {
            return this._Arrangement;
        }

        public void SetArrangement(Arrangement a)
        {
            this._Arrangement = a;
            this.ArrangementID = a.ID;
        }

        public string ArrangementID { get; set; }
        public string TouristID { get; set; }

        public string Text { get; set; }
        public short Rate
        {
            get
            {
                return Rate;
            }
            set
            {
                if (value >= 1 && value <= 5)
                {
                    Rate = value;
                }
                else
                {
                    throw new Exception("Value out of boundaries(1-5)");
                }
            }
        }
        public CommentStatus _CommentStatus { get; set; }
    }
}