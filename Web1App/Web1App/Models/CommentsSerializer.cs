﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Web1App.Models
{
    public class CommentsSerializer
    {
        private static readonly XmlSerializer serializer =
            new XmlSerializer(typeof(List<Comment>), new XmlRootAttribute("Comments"));
        private static readonly string path = System.AppContext.BaseDirectory + @"Xmls\Comments.xml";
        private static readonly object lockObj = new object();

        #region Serialization/Deserialization
        private static void Serialize(List<Comment> comments)
        {
            using (FileStream fs = new FileStream(path, FileMode.Truncate, FileAccess.Write, FileShare.None))
            {
                serializer.Serialize(fs, comments);
                fs.Close();
            }
        }

        private static List<Comment> Deserialize()
        {
            List<Comment> comments = new List<Comment>();

            if (File.Exists(path))
            {
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);

                // empty xml
                if (fs.Length == 0)
                {
                    fs.Close();
                    return comments;
                }

                //ucitavanje komentara
                comments = (List<Comment>)serializer.Deserialize(fs);
                if (comments == null)
                    comments = new List<Comment>();
                else
                    // komentar ce biti ucitan u aplikaciju samo ako je aranzman na koji se komentar
                    // odnosi aktivan odnosno nije logicki obrisan
                    comments = comments.FindAll(x => x.GetArrangement().Active);

                fs.Close();
            }

            return comments;
        }
        #endregion

        public void Create(Comment a)
        {
            lock (lockObj)
            {
                List<Comment> admins = Deserialize();
                admins.Add(a);
                Serialize(admins);
            }
        }

        public Comment Read(string comment_id)
        {
            Comment read = null;
            lock (lockObj)
            {
                read = Deserialize().Find(x => x.ID == comment_id);
            }
            return read;
        }
        public List<Comment> Read()
        {
            List<Comment> comments = null;
            lock (lockObj)
            {
                comments = Deserialize();
            }
            return comments;
        }

        public bool Update(Comment a)
        {
            bool updateSuccess = true;
            lock (lockObj)
            {
                List<Comment> comments = Deserialize();
                Comment old = comments.Find(x => x.ID == a.ID);
                if (old == null)
                    updateSuccess = false;
                else
                {
                    comments.Remove(old);
                    comments.Add(a);
                    Serialize(comments);
                }
            }
            return updateSuccess;
        }

        public bool Delete(string comment_id)
        {
            bool deleted = true;
            lock (lockObj)
            {
                List<Comment> comments = Deserialize();
                Comment a = comments.Find(x => x.ID == comment_id);
                if (a == null)
                    deleted = false;
                else
                {
                    comments.Remove(a);
                    Serialize(comments);
                }
            }
            return deleted;
        }
    }
}