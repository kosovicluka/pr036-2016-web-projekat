﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Web1App.Models
{
    public class TouristsSerializer
    {
        private static readonly XmlSerializer serializer =
            new XmlSerializer(typeof(List<Tourist>), new XmlRootAttribute("Tourists"));
        private static readonly string path = System.AppContext.BaseDirectory + @"Xmls\Tourists.xml";
        private static readonly object lockObj = new object();

        #region Serialization/Deserialization
        private static void Serialize(List<Tourist> tourists)
        {
            using (FileStream fs = new FileStream(path, FileMode.Truncate, FileAccess.Write, FileShare.None))
            {
                serializer.Serialize(fs, tourists);
                fs.Close();
            }
        }

        private static List<Tourist> Deserialize()
        {
            List<Tourist> tourists = new List<Tourist>();

            if (File.Exists(path))
            {
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);

                // empty xml
                if (fs.Length == 0)
                {
                    fs.Close();
                    return tourists;
                }

                //ucitavanje aranzmana
                tourists = (List<Tourist>)serializer.Deserialize(fs);
                if (tourists == null)
                    tourists = new List<Tourist>();
                else
                    // samo aktivni turisti ce biti ucitani u aplikaciju
                    tourists = tourists.FindAll(x => x.Active);

                fs.Close();
            }

            return tourists;
        }
        #endregion

        public static void Create(Tourist a)
        {
            lock (lockObj)
            {
                List<Tourist> tourists = Deserialize();
                tourists.Add(a);
                Serialize(tourists);
            }
        }

        public static Tourist Read(string username)
        {
            Tourist read = null;
            lock (lockObj)
            {
                read = Deserialize().Find(x => x.Username == username);
            }
            return read;
        }
        public static List<Tourist> Read()
        {
            List<Tourist> tourists = null;
            lock (lockObj)
            {
                tourists = Deserialize();
            }
            return tourists;
        }

        public static bool Update(Tourist a, string oldUsername)
        {
            bool updateSuccess = true;
            lock (lockObj)
            {
                List<Tourist> tourists = Deserialize();
                Tourist old = tourists.Find(x => x.Username == oldUsername);
                if (old == null)
                    updateSuccess = false;
                else
                {
                    tourists.Remove(old);
                    tourists.Add(a);
                    Serialize(tourists);
                }
            }
            return updateSuccess;
        }

        public static bool Delete(string username)
        {
            bool deleted = true;
            lock (lockObj)
            {
                List<Tourist> tourists = Deserialize();
                Tourist a = tourists.Find(x => x.Username == username);
                if (a == null)
                    deleted = false;
                else
                {
                    tourists.Remove(a);
                    Serialize(tourists);
                }
            }
            return deleted;
        }
    }
}