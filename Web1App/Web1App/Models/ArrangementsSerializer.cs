﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Web1App.Models
{
    public class ArrangementsSerializer
    {
        private static readonly XmlSerializer serializer =
            new XmlSerializer(typeof(List<Arrangement>), new XmlRootAttribute("Arrangements"));
        private static readonly string path = System.AppContext.BaseDirectory + @"Xmls\Arrangements.xml";
        private static readonly object lockObj = new object();

        #region Serialization/Deserialization
        private static void Serialize(List<Arrangement> arrangements)
        {
            using (FileStream fs = new FileStream(path, FileMode.Truncate, FileAccess.Write, FileShare.None))
            {
                serializer.Serialize(fs, arrangements);
                fs.Close();
            }
        }

        private static List<Arrangement> Deserialize()
        {
            List<Arrangement> arrangements = new List<Arrangement>();

            if (File.Exists(path))
            {
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);

                // empty xml
                if(fs.Length == 0)
                {
                    fs.Close();
                    return arrangements;
                }

                //ucitavanje aranzmana
                arrangements = (List<Arrangement>)serializer.Deserialize(fs);
                if (arrangements == null)
                    arrangements = new List<Arrangement>();
                else
                {
                    // samo aktivni aranzmani ce biti ucitani u aplikaciju
                    arrangements = arrangements.FindAll(x => x.Active);
                    foreach (var arr in arrangements)
                    {
                        arr._Accommodations = arr._Accommodations.FindAll(x => x.Active);
                        foreach (var acc in arr._Accommodations)
                        {
                            acc.Units = acc.Units.FindAll(x => x.Active);
                        }
                    }
                }
                fs.Close();
            }

            return arrangements;
        }
        #endregion

        #region Create
        public static bool Create(Arrangement a)
        {
            bool success = false;
            lock (lockObj)
            {
                List<Arrangement> arrangements = Deserialize();
                arrangements.Add(a);
                Serialize(arrangements);
                success = true;
            }
            return success;
        }

        public static bool CreateAccommodation(Accommodation a, string arrangement_id)
        {
            bool success = false;
            lock (lockObj)
            {
                List<Arrangement> arrangements = Deserialize();
                Arrangement arr = arrangements.Find(x => x.ID == arrangement_id);
                if (arr != null)
                {
                    arr._Accommodations.Add(a);
                    Serialize(arrangements);
                    success = true;
                }
            }
            return success;
        }

        public static bool CreateAccommodationUnit(AccommodationUnit a, string arrangement_id, string accommodation_id)
        {
            bool success = false;
            lock (lockObj)
            {
                List<Arrangement> arrangements = Deserialize();
                Arrangement arr = arrangements.Find(x => x.ID == arrangement_id);
                if (arr != null)
                {
                    Accommodation acc = arr._Accommodations.Find(x => x.ID == accommodation_id);
                    if (acc != null)
                    {
                        acc.Units.Add(a);
                        Serialize(arrangements);
                        success = true;
                    }
                }
            }
            return success;
        }
        #endregion

        #region Read
        public static Arrangement Read(string arrangement_id)
        {
            Arrangement read = null;
            lock (lockObj)
            {
                read = Deserialize().Find(x => x.ID == arrangement_id);
            }
            return read;
        }
        public static List<Arrangement> Read()
        {
            List<Arrangement> arrangements = null;
            lock (lockObj)
            {
                arrangements = Deserialize();
            }
            return arrangements;
        }

        public static Accommodation ReadAccommodation(string accommodation_id)
        {
            List<Arrangement> arrangements = null;
            lock (lockObj)
            {
                arrangements = Read();
            }
            Accommodation acc = null;
            foreach (var a in arrangements)
            {
                if ((acc = a._Accommodations.Find(x => x.ID == accommodation_id)) != null)
                    return acc;
            }
            return acc;
        }

        public static AccommodationUnit ReadAccommodationUnit(string accommodation_unit_id)
        {
            List<Arrangement> arrangements = null;
            lock (lockObj)
            {
                arrangements = Read();
            }
            AccommodationUnit au = null;
            foreach (var a in arrangements)
            {
                foreach (var acc in a._Accommodations)
                {
                    if ((au = acc.Units.Find(x => x.ID == accommodation_unit_id)) != null)
                        return au;
                }
            }
            return au;
        }
        #endregion

        #region Update
        public static bool Update(Arrangement a)
        {
            bool updateSuccess = true;
            lock (lockObj)
            {
                List<Arrangement> arrangements = Deserialize();
                Arrangement old = arrangements.Find(x => x.ID == a.ID);
                if (old == null)
                    updateSuccess = false;
                else
                {
                    arrangements.Remove(old);
                    arrangements.Add(a);
                    Serialize(arrangements);
                }
            }
            return updateSuccess;
        }

        public static bool UpdateAccommodation(Accommodation a)
        {
            bool updateSuccess = false;
            lock (lockObj)
            {
                string arrangement_id = "";
                List<Arrangement> arrangements = Deserialize();
                foreach (var arr in arrangements)
                {
                    foreach (var acc in arr._Accommodations)
                    {
                        if (acc.ID == a.ID)
                        {
                            arrangement_id = arr.ID;
                            updateSuccess = true;
                            break;
                        }
                    }
                    if (updateSuccess)
                        break;
                }
                if (updateSuccess)
                {
                    arrangements.Find(x => x.ID == arrangement_id)._Accommodations.RemoveAll(y => y.ID == a.ID);
                    arrangements.Find(x => x.ID == arrangement_id)._Accommodations.Add(a);
                    Serialize(arrangements);
                }
            }
            return updateSuccess;
        }

        public static bool UpdateAccommodationUnit(AccommodationUnit a)
        {
            bool updateSuccess = false;
            lock (lockObj)
            {
                string arrangement_id = "";
                string accommodation_id = "";
                bool found = false;
                List<Arrangement> arrangements = Deserialize();
                foreach (var arr in arrangements)
                {
                    foreach (var acc in arr._Accommodations)
                    {
                        foreach (var au in acc.Units)
                        {
                            if (au.ID == a.ID)
                            {
                                arrangement_id = arr.ID;
                                accommodation_id = acc.ID;
                                found = true;
                                break;
                            }
                        }
                        if (found)
                            break;
                    }
                    if (found)
                        break;
                }
                if (found)
                {
                    arrangements.Find(x => x.ID == arrangement_id)._Accommodations.Find(x => x.ID == accommodation_id).Units.RemoveAll(x => x.ID == a.ID);
                    arrangements.Find(x => x.ID == arrangement_id)._Accommodations.Find(x => x.ID == accommodation_id).Units.Add(a);
                    Serialize(arrangements);
                    updateSuccess = true;
                }
            }
            return updateSuccess;
        }
        #endregion

        #region Delete
        public static bool Delete(string arrangement_id)
        {
            bool deleted = true;
            lock (lockObj)
            {
                List<Arrangement> arrangements = Deserialize();
                Arrangement a = arrangements.Find(x => x.ID == arrangement_id);
                if (a == null)
                    deleted = false;
                else
                {
                    a.Active = false;
                    Serialize(arrangements);
                }
            }
            return deleted;
        }

        public static bool DeleteAccommodation(string accommodation_id)
        {
            bool deleted = false;
            lock (lockObj)
            {
                List<Arrangement> arrangements = Deserialize();

                foreach (var arr in arrangements)
                {
                    foreach (var acc in arr._Accommodations)
                    {
                        if (acc.ID == accommodation_id)
                        {
                            acc.Active = false;
                            Serialize(arrangements);
                            deleted = true;
                            break;
                        }
                    }
                    if (deleted)
                        break;
                }
            }
            return deleted;
        }

        public static bool DeleteAccommodationUnit(string accommodation_unit_id)
        {
            bool deleted = false;
            lock (lockObj)
            {
                List<Arrangement> arrangements = Deserialize();

                foreach (var arr in arrangements)
                {
                    foreach (var acc in arr._Accommodations)
                    {
                        foreach (var au in acc.Units)
                        {
                            if (au.ID == accommodation_unit_id)
                            {
                                au.Active = false;
                                Serialize(arrangements);
                                deleted = true;
                                break;
                            }
                        }
                        if (deleted)
                            break;
                    }
                    if (deleted)
                        break;
                }
            }
            return deleted;
        }
        #endregion
    }
}