﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Web1App.Models
{
    public class UserValidator
    {
        public static bool ValidateUser(User user)
        {
            return ValidatePassword(user.Password) &&
                ValidateName(user.Name) &&
                ValidateSurname(user.Surname) &&
                ValidateEmail(user.Email) &&
                ValidateDate(user.Date);
        }

        private static bool ValidateUsername(string username)
        {
            if (username != "")
            {
                List<User> admins = AdministratorsSerializer.Read().Cast<User>().ToList();
                List<User> managers = ManagersSerializer.Read().Cast<User>().ToList();
                List<User> tourists = TouristsSerializer.Read().Cast<User>().ToList();
                List<User> allUsers = new List<User>();
                allUsers.AddRange(admins);
                allUsers.AddRange(managers);
                allUsers.AddRange(tourists);
                if ((allUsers.Find(x => x.Username == username)) == null)
                {
                    Regex regex = new Regex(@"^[a-zA-Z0-9]+");
                    if (regex.IsMatch(username))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static bool ValidatePassword(string password)
        {
            if (password.Length >= 8)
            {
                return true;
            }
            return false;
        }

        private static bool ValidateName(string name)
        {
            if (name != "")
            {
                Regex regex = new Regex(@"^[A-Za-z]+");
                if (regex.IsMatch(name))
                {
                    return true;
                }
            }
            return false;
        }

        private static bool ValidateSurname(string surname)
        {
            return ValidateName(surname);
        }

        private static bool ValidateEmail(string email)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            if (regex.IsMatch(email))
            {
                return true;
            }
            return false;
        }

        private static bool ValidateDate(string date)
        {
            Regex regex = new Regex(@"^(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d$");
            if (regex.IsMatch(date))
            {
                return true;
            }
            return false;
        }
    }
}