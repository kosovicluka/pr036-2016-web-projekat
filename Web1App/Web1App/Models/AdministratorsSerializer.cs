﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Web1App.Models
{
    public class AdministratorsSerializer
    {

        private static readonly XmlSerializer serializer = 
            new XmlSerializer(typeof(List<Administrator>), new XmlRootAttribute("Administrators"));
        private static readonly string path = System.AppContext.BaseDirectory + @"Xmls\Administrators.xml";
        private static readonly object lockObj = new object();

        #region Serialization/Deserialization
        private static void Serialize(List<Administrator> administrators)
        {
            using (FileStream fs = new FileStream(path, FileMode.Truncate, FileAccess.Write, FileShare.None))
            {
                serializer.Serialize(fs, administrators);
                fs.Close();
            }
        }

        private static List<Administrator> Deserialize()
        {
            List<Administrator> administrators = new List<Administrator>();

            if (File.Exists(path))
            {
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);

                // empty xml
                if (fs.Length == 0)
                {
                    fs.Close();
                    return administrators;
                }

                //ucitavanje administratora
                administrators = (List<Administrator>)serializer.Deserialize(fs);
                if (administrators == null)
                    administrators = new List<Administrator>();

                fs.Close();
            }

            return administrators;
        }
        #endregion

        public static void Create(Administrator a)
        {
            lock (lockObj)
            {
                List<Administrator> admins = Deserialize();
                admins.Add(a);
                Serialize(admins);
            }
        }

        public static Administrator Read(string username)
        {
            Administrator read = null;
            lock (lockObj)
            {
                read = Deserialize().Find(x => x.Username == username);
            }
            return read;
        }
        public static List<Administrator> Read()
        {
            List<Administrator> admins = null;
            lock (lockObj)
            {
                admins = Deserialize();
            }
            return admins;
        }

        public static bool Update(Administrator a, string oldUsername)
        {
            bool updateSuccess = true;
            lock (lockObj)
            {
                List<Administrator> admins = Deserialize();
                Administrator old = admins.Find(x => x.Username == oldUsername);
                if (old == null)
                    updateSuccess = false;
                else
                {
                    admins.Remove(old);
                    admins.Add(a);
                    Serialize(admins);
                }
            }
            return updateSuccess;
        }

        public static bool Delete(string username)
        {
            bool deleted = true;
            lock (lockObj)
            {
                List<Administrator> admins = Deserialize();
                Administrator a = admins.Find(x => x.Username == username);
                if (a == null)
                    deleted = false;
                else
                {
                    admins.Remove(a);
                    Serialize(admins);
                }
            }
            return deleted;
        }

    }
}