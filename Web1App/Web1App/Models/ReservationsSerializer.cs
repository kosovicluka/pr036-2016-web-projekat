﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Web1App.Models
{
    public class ReservationsSerializer
    {
        private static readonly XmlSerializer serializer =
             new XmlSerializer(typeof(List<Reservation>), new XmlRootAttribute("Reservations"));
        private static readonly string path = System.AppContext.BaseDirectory + @"Xmls\Reservations.xml";
        private static readonly object lockObj = new object();

        #region Serialization/Deserialization
        private static void Serialize(List<Reservation> reservations)
        {
            using (FileStream fs = new FileStream(path, FileMode.Truncate, FileAccess.Write, FileShare.None))
            {
                serializer.Serialize(fs, reservations);
                fs.Close();
            }
        }

        private static List<Reservation> Deserialize()
        {
            List<Reservation> reservations = new List<Reservation>();

            if (File.Exists(path))
            {
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);

                // empty xml
                if (fs.Length == 0)
                {
                    fs.Close();
                    return reservations;
                }

                //ucitavanje rezervacija
                reservations = (List<Reservation>)serializer.Deserialize(fs);
                if (reservations == null)
                    reservations = new List<Reservation>();
                else
                    reservations = reservations.FindAll(x => x.Active);

                fs.Close();
            }

            return reservations;
        }
        #endregion

        public static void Create(Reservation a)
        {
            lock (lockObj)
            {
                List<Reservation> reservations = Deserialize();
                reservations.Add(a);
                Serialize(reservations);
            }
        }

        public static Reservation Read(string arrangement_id)
        {
            Reservation read = null;
            lock (lockObj)
            {
                read = Deserialize().Find(x => x.ID == arrangement_id);
            }
            return read;
        }
        public static List<Reservation> Read()
        {
            List<Reservation> reservations = null;
            lock (lockObj)
            {
                reservations = Deserialize();
            }
            return reservations;
        }

        public static bool Update(Reservation a)
        {
            bool updateSuccess = true;
            lock (lockObj)
            {
                List<Reservation> reservations = Deserialize();
                Reservation old = reservations.Find(x => x.ID == a.ID);
                if (old == null)
                    updateSuccess = false;
                else
                {
                    reservations.Remove(old);
                    reservations.Add(a);
                    Serialize(reservations);
                }
            }
            return updateSuccess;
        }

        public static bool Delete(string arrangement_id)
        {
            bool deleted = true;
            lock (lockObj)
            {
                List<Reservation> reservations = Deserialize();
                Reservation a = reservations.Find(x => x.ID == arrangement_id);
                if (a == null)
                    deleted = false;
                else
                {
                    reservations.Remove(a);
                    Serialize(reservations);
                }
            }
            return deleted;
        }
    }
}