﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Web1App.Models
{
    public enum ArrangementType
    {
        BB,
        HALFBOARD,
        FULLBOARD,
        ALLINCLUSIVE,
        RENTAPARTMENT,
    }

    public enum Transport
    {
        BUS,
        AIRPLANE,
        BUSNAIRPLANE,
        INDIVIDUAL,
        OTHER
    }

    public class Arrangement
    {
        public Arrangement()
        {
            ID = Guid.NewGuid().ToString();
        }
        public Arrangement(string name, ArrangementType arrangementType, Transport transport,
            string location, string startDate, string endDate, PlaceOfDeparture placeOfDeparture,
            string timeOfDeparture, int maxNumOfPassengers, string arrangementDesc, 
            string travelDesc, string arrangementPoster)
        {
            Name = name;
            _ArrangementType = arrangementType;
            _Transport = transport;
            Location = location;
            StartDate = startDate;
            EndDate = endDate;
            _PlaceOfDeparture = placeOfDeparture;
            TimeOfDeparture = timeOfDeparture;
            MaxNumOfPassengers = maxNumOfPassengers;
            ArrangementDescription = arrangementDesc;
            TravelDescription = travelDesc;
            Poster = arrangementPoster;
            Active = true;
            ID = Guid.NewGuid().ToString();
        }

        [Required, RegularExpression("/^[A-Za-z]+$/")]
        public string Name { get; set; }
        [Required]
        public ArrangementType _ArrangementType { get; set; }
        [Required]
        public Transport _Transport { get; set; }

        [Required, RegularExpression("/^[A-Za-z]+$/")]
        public string Location { get; set; }
        [Required]
        public string StartDate { get; set; } // dd/MM/yyyy
        [Required]
        public string EndDate { get; set; } // dd/MM/yyyy
        [Required]
        public PlaceOfDeparture _PlaceOfDeparture { get; set; }
        [Required]
        public string TimeOfDeparture { get; set; }
        [Required]
        public int MaxNumOfPassengers { get; set; }
        [Required]
        public string ArrangementDescription { get; set; }
        [Required]
        public string TravelDescription { get; set; }
        [Required]
        public string Poster { get; set; }
        public List<Accommodation> _Accommodations { get; set; } = new List<Accommodation>();
        public List<Comment> Comments { get; set; } = new List<Comment>();
        public bool Active { get; set; } = true;
        public string ID { get; set; }
        public string GetStartDateForPresentation()
        {
            string[] newDateArr = this.StartDate.Split('/');
            return newDateArr[2] + "/" + newDateArr[1] + "/" + newDateArr[0];
        }
        public string GetEndDateForPresentation()
        {
            string[] newDateArr = this.EndDate.Split('/');
            return newDateArr[2] + "/" + newDateArr[1] + "/" + newDateArr[0];
        }

        public void SetStartDate(string value)
        {
            if (value == null || value == "")
                return;
            string[] parts = value.Split('-');
            this.StartDate = parts[2] + "/" + parts[1] + "/" + parts[0];
        }

        public void SetEndDate(string value)
        {
            if (value == null || value == "")
                return;
            string[] parts = value.Split('-');
            this.EndDate = parts[2] + "/" + parts[1] + "/" + parts[0];
        }

        public string ConvertDateForView(string date, string separator)
        {
            string[] parts = date.Split('/');
            return parts[2] + separator + parts[1] + separator + parts[0];
        }

        public List<SelectListItem> BindTransport()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "Bus",
                Value = Transport.BUS.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Airplane",
                Value = Transport.AIRPLANE.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Bus&Airplane",
                Value = Transport.BUSNAIRPLANE.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Individual",
                Value = Transport.INDIVIDUAL.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Other",
                Value = Transport.OTHER.ToString()
            });
            SelectListItem item = items.Find(x => x.Value == this._Transport.ToString());
            item.Selected = true;

            return items;
        }
        public List<SelectListItem> BindArrangementType()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "BB",
                Value = ArrangementType.BB.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Half Board",
                Value = ArrangementType.HALFBOARD.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Full Board",
                Value = ArrangementType.FULLBOARD.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "All Inclusive",
                Value = ArrangementType.ALLINCLUSIVE.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Rent Apartment",
                Value = ArrangementType.RENTAPARTMENT.ToString()
            });
            SelectListItem item = items.Find(x => x.Value == this._ArrangementType.ToString());
            item.Selected = true;

            return items;
        }

        public int CalculateDays()
        {
            DateTime start = StringToDate(StartDate);
            DateTime end = StringToDate(EndDate);
            return (end - start).Days;
        }

        public DateTime StringToDate(string date)
        {
            //expect string in dd/MM/yyyy format
            if (date.Contains('/'))
            {
                string[] parts = date.Split('/');
                return new DateTime(
                Convert.ToInt32(parts[2]),
                Convert.ToInt32(parts[1]),
                Convert.ToInt32(parts[0])
                );
            }
            else if(date.Contains('-'))
            {
                string[] parts = date.Split('-');
                return new DateTime(
                Convert.ToInt32(parts[0]),
                Convert.ToInt32(parts[1]),
                Convert.ToInt32(parts[2])
                );
            }
            return new DateTime();
        }

        public double FindMinimalPrice()
        {
            List<AccommodationUnit> allUnits = new List<AccommodationUnit>();
            for (int i = 0; i < _Accommodations.Count; i++)
            {
                allUnits.AddRange(_Accommodations[i].GetActiveUnits());
            }
            if (allUnits.Count > 0)
                return allUnits.Min(x => x.Price);
            return 0;
        }

        public bool UpcomingArrangement()
        {
            if (this.StringToDate(this.StartDate).Ticks > DateTime.Now.Ticks)
            {
                return true;
            }
            return false;
        }

        public bool CheckDates(string startlow, string starthigh, string endlow, string endhigh)
        {
            DateTime start = StringToDate(StartDate);
            DateTime end = StringToDate(EndDate);
            if (startlow != "")
            {
                DateTime startl = StringToDate(startlow);
                if (!(startl.Ticks <= start.Ticks))
                    return false;
            }
            if (starthigh != "")
            {
                DateTime starth = StringToDate(starthigh);
                if (!(starth.Ticks >= start.Ticks))
                    return false;
            }
            if (endlow != "")
            {
                DateTime endl = StringToDate(endlow);
                if (!(endl.Ticks <= end.Ticks))
                    return false;
            }
            if (endhigh != "")
            {
                DateTime endh = StringToDate(endhigh);
                if (!(endh.Ticks >= end.Ticks))
                    return false;
            }
            return true;
        }

    }
}