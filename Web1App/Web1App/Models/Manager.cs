﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web1App.Models
{
    public class Manager : User
    {
        public Manager()
        {

        }

        public Manager(string username, string password,
            string name, string surname, string email, string date,
            Gender gender) : base(username, password,
                name, surname, email, date, gender, Role.MANAGER)
        {

        }

        public bool Active { get; set; } = true;

        public List<string> ArrangementsIDs { get; set; } = new List<string>();
        public List<string> ReservationsIDs { get; set; } = new List<string>();


        #region Arrangements
        private List<Arrangement> Arrangements { get; set; } = new List<Arrangement>();

        public void SetArrangements(List<Arrangement> arrangements)
        {
            this.Arrangements = arrangements;
            foreach (var a in this.Arrangements)
            {
                this.ArrangementsIDs.Add(a.ID);
            }
        }

        public List<Arrangement> GetArrangements()
        {
            return this.Arrangements;
        }

        public void AddArrangement(Arrangement a)
        {
            this.Arrangements.Add(a);
            if (!this.ArrangementsIDs.Contains(a.ID))
            {
                this.ArrangementsIDs.Add(a.ID);
            }
        }

        public List<Arrangement> GetActiveArrangements()
        {
            List<Arrangement> arrs = Arrangements.FindAll(x => x.Active);
            if (arrs == null)
                return Arrangements;
            return arrs;
        }

        public List<Arrangement> GetInactiveArrangements()
        {
            List<Arrangement> arrs = Arrangements.FindAll(x => !x.Active);
            if (arrs == null)
                return Arrangements;
            return arrs;
        }
        #endregion

        #region Reservations
        private List<Reservation> Reservations { get; set; } = new List<Reservation>();

        public void SetReservations(List<Reservation> reservations)
        {
            this.Reservations = reservations;
            foreach (var r in this.Reservations)
            {
                this.ReservationsIDs.Add(r.ID);
            }
        }

        public List<Reservation> GetReservations()
        {
            return this.Reservations;
        }

        public void AddReservation(Reservation r)
        {
            this.Reservations.Add(r);
            if (!this.ReservationsIDs.Contains(r.ID))
            {
                this.ReservationsIDs.Add(r.ID);
            }
        } 

        #endregion


    }
}