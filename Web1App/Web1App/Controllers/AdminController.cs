﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web1App.Models;
using Web1App.ViewModelComponents;
using Web1App.ViewModels;

namespace Web1App.Controllers
{
    public class AdminController : Controller
    {
        #region HomePages
        public ActionResult Home()
        {
            if (!AuthorizationCheck(Session["user"] as User))
            {
                return NavigateToUserController(Session["user"] as User);
            }
            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                Title = "Future Arrangements",
                ArrangementsSearch = new ArrangementsSearch(),
                ArrangementsSort = new ArrangementsSort(),
            };
            try
            {
                home.Arrangements = ArrangementsSerializer.Read().FindAll
                        (x => x.StringToDate(x.StartDate).Ticks > DateTime.Now.Ticks);
            }
            catch (Exception)
            {
                home.Arrangements = new List<Arrangement>();
                ViewBag.message = "Data currently unavailable";
            }
            return View(home);
        }

        public ActionResult PastAndCurrentArrangements()
        {
            if (!AuthorizationCheck(Session["user"] as User))
            {
                return NavigateToUserController(Session["user"] as User);
            }
            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                Title = "Past And Current Arrangements",
                ArrangementsSearch = new ArrangementsSearch(),
                ArrangementsSort = new ArrangementsSort(),
            };
            try
            {
                home.Arrangements = ArrangementsSerializer.Read().FindAll
                    (x => x.StringToDate(x.StartDate).Ticks <= DateTime.Now.Ticks);
            }
            catch (Exception)
            {
                home.Arrangements = new List<Arrangement>();
                ViewBag.message = "Data currently unavailable";
            }
            return View(home);
        }

        public ActionResult AllArrangements()
        {
            if (!AuthorizationCheck(Session["user"] as User))
            {
                return NavigateToUserController(Session["user"] as User);
            }
            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                Title = "All Arrangements",
                ArrangementsSearch = new ArrangementsSearch(),
                ArrangementsSort = new ArrangementsSort(),
            };
            try
            {
                home.Arrangements = ArrangementsSerializer.Read();
            }
            catch (Exception)
            {
                home.Arrangements = new List<Arrangement>();
                ViewBag.message = "Data currently unavailable";
            }
            return View(home);
        }

        [HttpGet]
        public ActionResult SearchSortFutureArrangements(){

            // authorization check
            if (!AuthorizationCheck(Session["user"] as User))
            {
                return NavigateToUserController(Session["user"] as User);
            }

            // search or sort
            return SearchSortArrangements("Future Arrangements");
        }
        [HttpGet]
        public ActionResult SearchSortPastAndCurrentArrangements()
        {
            // authorization check
            if (!AuthorizationCheck(Session["user"] as User))
            {
                return NavigateToUserController(Session["user"] as User);
            }

            // search or sort
            return SearchSortArrangements("Past And Current Arrangements");
        }
        [HttpGet]
        public ActionResult SearchSortAllArrangements()
        {
            // authorization check
            if (!AuthorizationCheck(Session["user"] as User))
            {
                return NavigateToUserController (Session["user"] as User);
            }

            // search or sort
            return SearchSortArrangements("All Arrangements");
        }

        public ActionResult SearchSortArrangements(string title)
        {

            HomeArrangementsViewModel vm = new HomeArrangementsViewModel()
            {
                Arrangements = new List<Arrangement>(),
                ArrangementsSearch = new ArrangementsSearch()
                {
                    StartDateFrom = Request["startDateFrom"],
                    StartDateTo = Request["startDateTo"],
                    EndDateFrom = Request["endDateFrom"],
                    EndDateTo = Request["endDateTo"],
                    Name = Request["name"],
                },
                ArrangementsSort = new ArrangementsSort(),
                Title = title,
            };

            try
            {

                vm.ArrangementsSearch.Transport =
                    (Transport)Enum.Parse(typeof(Transport), Request["transport"].ToUpper());
                vm.ArrangementsSearch.ArrangementType =
                    (ArrangementType)Enum.Parse(typeof(ArrangementType), Request["arrangementType"].ToUpper());
                vm.ArrangementsSort.Option = (ArrangementsSortOption)Enum.Parse(typeof(ArrangementsSortOption), Request["arr_sort"].ToUpper());
            }
            catch (ArgumentNullException)
            {

                ViewBag.message = "Error parsing form data : Missing data from the form";
            }
            catch (ArgumentException)
            {

                ViewBag.message = "Error parsing form data : Invalid form data";
            }
            catch (Exception)
            {

                ViewBag.message = "Error parsing form data";
            }

            try{

                // read {title} arrangements from database
                vm.Arrangements = ReadArrangements(title);
            }
            catch (NullReferenceException){

                ViewBag.message = "Data currently unavailable";
                return View("Home", vm);
            }
            catch (ArgumentNullException){

                ViewBag.message = "Error processing data";
                return View("Home", vm);
            }
            catch (Exception){

                ViewBag.message = "Internal server error";
                return View("Home", vm);
            }

            try
            {

                TempData["arrs"] = vm.Arrangements;

                if (Request["Search"] != null)
                    vm.Arrangements = Search(vm.Arrangements, vm.ArrangementsSearch);
                else
                    vm.Arrangements = Sort(vm.Arrangements, vm.ArrangementsSort);
            }
            catch (Exception)
            {
                vm.Arrangements = (List<Arrangement>)TempData["arrs"];
                ViewBag.message = "Error searching/sorting data : The data shown probably does not meet the search or sort criteria";
            }

            return View("Home", vm);
        }
        public List<Arrangement> Search(List<Arrangement> arrangements, ArrangementsSearch search)
        {
            if (search.StartDateFrom != null && search.StartDateFrom != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.StartDate).Ticks >= x.StringToDate(search.StartDateFrom).Ticks);
            }
            if (search.StartDateTo != null && search.StartDateTo != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.StartDate).Ticks <= x.StringToDate(search.StartDateTo).Ticks);
            }
            if (search.EndDateFrom != null && search.EndDateFrom != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.EndDate).Ticks >= x.StringToDate(search.EndDateFrom).Ticks);
            }
            if (search.EndDateTo != null && search.EndDateTo != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.EndDate).Ticks <= x.StringToDate(search.EndDateTo).Ticks);
            }
            if (search.Name != null)
            {
                arrangements = arrangements.FindAll(x => x.Name.ToUpper().Contains(search.Name.ToUpper()));
            }
            arrangements = arrangements.
                FindAll(x => x._ArrangementType == search.ArrangementType && x._Transport == search.Transport);
            
            return arrangements;
        }
        public List<Arrangement> Sort(List<Arrangement> arrangements, ArrangementsSort arrangementsSort) {

            switch (arrangementsSort.Option)
            {
                case ArrangementsSortOption.NAME_A:
                    return arrangements.OrderBy(x => x.Name).ToList();
                case ArrangementsSortOption.NAME_D:
                    return arrangements.OrderByDescending(x => x.Name).ToList();
                case ArrangementsSortOption.STARTDATE_A:
                    return arrangements.OrderBy(x => x.StringToDate(x.StartDate)).ToList();
                case ArrangementsSortOption.STARTDATE_D:
                    return arrangements.OrderByDescending(x => x.StringToDate(x.StartDate)).ToList();
                case ArrangementsSortOption.ENDDATE_A:
                    return arrangements.OrderBy(x => x.StringToDate(x.EndDate)).ToList();
                case ArrangementsSortOption.ENDDATE_D:
                    return arrangements.OrderByDescending(x => x.StringToDate(x.EndDate)).ToList();
                default:
                    return arrangements;
            }
        }

        #endregion

        #region Arrangement

        [HttpGet]
        public ActionResult SelectArrangement()
        {
            if (!AuthorizationCheck(Session["user"] as User)) {
                return NavigateToUserController(Session["user"] as User);
            }

            ArrangementViewModel arrangementViewModel = new ArrangementViewModel
            {
                AccommodationSearch = new AccommodationSearch(),
                AccommodationSort = new AccommodationSort()
            };

            try{

                arrangementViewModel.Arrangement = 
                    ArrangementsSerializer.Read().Find(x => x.ID == Request["arrangement_id"]);
            }
            catch (Exception)
            {
                ViewBag.message = "Internal server error: Arrangement not found";
                return RedirectToAction("Home");
            }
            return View("Arrangement", arrangementViewModel);
        }

        [HttpGet]
        public ActionResult SearchSortAccommodations()
        {
            if (!AuthorizationCheck(Session["user"] as User))
            {
                return NavigateToUserController(Session["user"] as User);
            }

            ArrangementViewModel arrangementModelView = new ArrangementViewModel() { 
                AccommodationSearch = new AccommodationSearch() {
                    Name = Request["acc_name"],
                    Pool = Request["pool"] != null,
                    Spa = Request["spa"] != null,
                    AdaptedForPWDs = Request["pwd"] != null,
                    WiFi = Request["wifi"] != null
                },
                Arrangement = new Arrangement(),
                AccommodationSort = new AccommodationSort()
            };

            try
            {
                arrangementModelView.Arrangement = ArrangementsSerializer.Read(Request["arrangement_id"]);
            }
            catch (Exception)
            {
                ViewBag.message = "Internal server error: Arrangement not found";
                return RedirectToAction("Home");
            }

            try
            {
                arrangementModelView.AccommodationSearch.AccommodationType =
                    (AccommodationType)Enum.Parse(typeof(AccommodationType), Request["acc_type"].ToUpper());
                arrangementModelView.AccommodationSort.Option =
                    (AccommodationSortOption)Enum.Parse(typeof(AccommodationSortOption), Request["accommodation_sort"].ToUpper());
            }
            catch (Exception)
            {
                ViewBag.message = "Error parsing data";
                return View("Arrangement", arrangementModelView);
            }

            try
            {
                TempData["accs"] = arrangementModelView.Arrangement._Accommodations;
                if (Request["search"] != null)
                    arrangementModelView.Arrangement._Accommodations = SearchAccommodations(arrangementModelView.Arrangement, arrangementModelView.AccommodationSearch);
                else
                    arrangementModelView.Arrangement._Accommodations = SortAccommodations(arrangementModelView.Arrangement, arrangementModelView.AccommodationSort);
            }
            catch (Exception)
            {
                arrangementModelView.Arrangement._Accommodations = (List<Accommodation>)TempData["accs"];
                ViewBag.message = "Data currently unavailable";
            }

            return View("Arrangement", arrangementModelView);
        }


        public List<Accommodation> SortAccommodations(Arrangement arrangement, AccommodationSort accommodationSort)
        {
            switch (accommodationSort.Option)
            {
                case AccommodationSortOption.NAME_A:
                    return arrangement._Accommodations.OrderBy(x => x.Name).ToList();
                case AccommodationSortOption.NAME_D:
                    return arrangement._Accommodations.OrderByDescending(x => x.Name).ToList();
                case AccommodationSortOption.UNITS_A:
                    return arrangement._Accommodations.OrderBy(x => x.TotalNumberOfUnits).ToList();
                case AccommodationSortOption.UNITS_D:
                    return arrangement._Accommodations.OrderByDescending(x => x.TotalNumberOfUnits).ToList();
                case AccommodationSortOption.FREE_UNITS_A:
                    return arrangement._Accommodations.OrderBy(x => x.NumberOfFreeUnits).ToList();
                case AccommodationSortOption.FREE_UNITS_D:
                    return arrangement._Accommodations.OrderByDescending(x => x.NumberOfFreeUnits).ToList();
                default:
                    return arrangement._Accommodations;
            }
        }

        public List<Accommodation> SearchAccommodations(Arrangement arrangement, AccommodationSearch accommodationSearch)
        {
            List<Accommodation> accommodations = arrangement._Accommodations;

            accommodations = arrangement._Accommodations.FindAll
                (x => x._AccommodationType.ToString() == Request["acc_type"]);
            
            if (accommodationSearch.Name != null)
            {
                accommodations = accommodations.FindAll(x => x.Name.ToUpper().Contains(Request["acc_name"].ToUpper()));
            }
            if (accommodationSearch.Pool)
            {
                accommodations = accommodations.FindAll(x => x.Pool);
            }
            if (accommodationSearch.Spa)
            {
                accommodations = accommodations.FindAll(x => x.Spa);
            }
            if (accommodationSearch.WiFi)
            {
                accommodations = accommodations.FindAll(x => x.WiFi);
            }
            if (accommodationSearch.AdaptedForPWDs)
            {
                accommodations = accommodations.FindAll(x => x.AdaptedForDisabledPeople);
            }
            return accommodations;
        }

        #endregion

        #region Accommodation

        [HttpPost]
        public ActionResult SelectAccommodation()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Index", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        break;
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            AccommodationViewModel viewModel = new AccommodationViewModel()
            {
                ArrangementId = Request["arrangement_id"],
                Accommodation = ArrangementsSerializer.Read(Request["arrangement_id"])._Accommodations.Find(x => x.ID == Request["accommodation_id"]),
                AccommodationUnitsSearch = new AccommodationUnitsSearch(),
                AccommodationUnitsSort = new AccommodationUnitsSort()
            };
            return View("Accommodation", viewModel);
        }

        [HttpPost]
        public ActionResult SearchSortAccommodationUnits()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Index", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        break;
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            AccommodationViewModel viewModel = new AccommodationViewModel()
            {
                Accommodation = ArrangementsSerializer.Read(Request["arrangement_id"])._Accommodations.Find(x => x.ID == Request["accommodation_id"]),
                ArrangementId = Request["arrangement_id"],
                AccommodationUnitsSearch = new AccommodationUnitsSearch()
                {
                    MinPrice = Convert.ToInt32(Request["minprice"]),
                    MaxPrice = Convert.ToInt32(Request["maxprice"]),
                    PetFree = Request["pets"] != null,
                    MinNumberOfGuests = Convert.ToInt32(Request["minNumOfGuests"]),
                    MaxNumberOfGuests = Convert.ToInt32(Request["maxNumOfGuests"]),
                },
                AccommodationUnitsSort = new AccommodationUnitsSort((AccommodationUnitsSortOption)Enum.Parse(typeof(AccommodationUnitsSortOption), Request["accommodationUnitsSort"].ToUpper()))
            };
            if (Request["search"] != null)
                viewModel.Accommodation.Units = SearchAccommodationUnits(viewModel.Accommodation.Units, viewModel.AccommodationUnitsSearch);
            else
                viewModel.Accommodation.Units = SortAccommodationUnits(viewModel.Accommodation.Units, viewModel.AccommodationUnitsSort);
            return View("Accommodation", viewModel);
        }

        public List<AccommodationUnit> SearchAccommodationUnits(List<AccommodationUnit> units, AccommodationUnitsSearch search)
        {
            if (search.MinPrice != 0)
            {
                units = units.FindAll(x => x.Price >= search.MinPrice);
            }
            if (search.MaxPrice != 0)
            {
                units = units.FindAll(x => x.Price <= search.MaxPrice);
            }
            if (search.PetFree)
            {
                units = units.FindAll(x => x.Pets);
            }
            if (search.MinNumberOfGuests != 0)
            {
                units = units.FindAll(x => x.MaxNumOfGuests >= search.MinNumberOfGuests);
            }
            if (search.MaxNumberOfGuests != 0)
            {
                units = units.FindAll(x => x.MaxNumOfGuests <= search.MaxNumberOfGuests);
            }
            return units;
        }

        public List<AccommodationUnit> SortAccommodationUnits(List<AccommodationUnit> units, AccommodationUnitsSort sort)
        {
            switch (sort.Option)
            {
                case AccommodationUnitsSortOption.ANOG_A:
                    return units.OrderBy(x => x.MaxNumOfGuests).ToList();
                case AccommodationUnitsSortOption.ANOG_D:
                    return units.OrderByDescending(x => x.MaxNumOfGuests).ToList();
                case AccommodationUnitsSortOption.PRICE_A:
                    return units.OrderBy(x => x.Price).ToList();
                case AccommodationUnitsSortOption.PRICE_D:
                    return units.OrderByDescending(x => x.Price).ToList();
                default:
                    return units;
            }
        }


        #endregion

        #region AccommodationUnit

        [HttpPost]
        public ActionResult SelectAccommodationUnit()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Index", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        break;
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            AccommodationUnitViewModel viewModel = new AccommodationUnitViewModel()
            {
                ArrangementId = Request["arrangement_id"],
                AccommodationId = Request["accommodation_id"],
                AccommodationUnit = ArrangementsSerializer.Read
                (Request["arrangement_id"])._Accommodations.Find
                (x => x.ID == Request["accommodation_id"]).Units.Find
                (x => x.ID == Request["accommodation_unit_id"])
            };
            return View("AccommodationUnit", viewModel);
        }

        #endregion

        #region NewManager
        public ActionResult NewManager()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        break;
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            ViewBag.Success = "";
            return View();
        }

        [HttpPost]
        public ActionResult NewManagerRequest()
        {
            Manager newManager = new Manager(
                Request["username"],
                Request["password"],
                Request["name"],
                Request["surname"],
                Request["email"],
                Request["date"],
                (Gender)Enum.Parse(typeof(Gender), Request["gender"].ToUpper())
                );

            // validation 
            if(UserValidator.ValidateUser(newManager as User))
            {
                ManagersSerializer.Create(newManager);

                ViewBag.Success = true;
            }
            else
            {
                ViewBag.Success = false;
            }

            return View("NewManager");
        }
        #endregion

        #region AllUsers
        public ActionResult AllUsers()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Index", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        break;
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            List<User> admins = AdministratorsSerializer.Read().Cast<User>().ToList();
            List<User> managers = ManagersSerializer.Read().Cast<User>().ToList();
            List<User> tourists = TouristsSerializer.Read().Cast<User>().ToList();
            List<User> allUsers = new List<User>();
            allUsers.AddRange(admins);
            allUsers.AddRange(managers);
            allUsers.AddRange(tourists);

            AllUsersViewModel viewModel = new AllUsersViewModel() { 
                Users = allUsers,
                Search = new UsersSearch(),
                Sort = new UsersSort(),
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult SearchSortAllUsers()
        {
            List<User> admins = AdministratorsSerializer.Read().Cast<User>().ToList();
            List<User> managers = ManagersSerializer.Read().Cast<User>().ToList();
            List<User> tourists = TouristsSerializer.Read().Cast<User>().ToList();
            List<User> allUsers = new List<User>();
            allUsers.AddRange(admins);
            allUsers.AddRange(managers);
            allUsers.AddRange(tourists);

            AllUsersViewModel viewModel = new AllUsersViewModel()
            {
                Users = allUsers,
                Search = new UsersSearch(
                    Request["name"],
                    Request["surname"],
                    (Role)Enum.Parse(typeof(Role), Request["role"].ToUpper())
                ),
                Sort = new UsersSort((UsersSortOption)Enum.Parse(typeof(UsersSortOption), Request["users_sort"].ToUpper())),
            };

            if (Request["search"] != null)
            {
                viewModel.Users = SearchAllUsers(viewModel.Users, viewModel.Search);
            }
            else
            {
                viewModel.Users = SortAllUsers(viewModel.Users, viewModel.Sort);
            }

            return View("AllUsers", viewModel);
        }

        public List<User> SearchAllUsers(List<User> allUsers, UsersSearch search)
        {
            return allUsers.FindAll(x =>
                x.Name.ToUpper().Contains(search.Name.ToUpper()) &&
                x.Surname.ToUpper().Contains(search.Surname.ToUpper()) &&
                x._Role == search.Role
            );
        }

        public List<User> SortAllUsers(List<User> allUsers, UsersSort sort)
        {
            switch (sort.Option)
            {
                case UsersSortOption.NAME_A:
                    return allUsers.OrderBy(x => x.Name).ToList();
                case UsersSortOption.NAME_D:
                    return allUsers.OrderByDescending(x => x.Name).ToList();
                case UsersSortOption.SURNAME_A:
                    return allUsers.OrderBy(x => x.Surname).ToList();
                case UsersSortOption.SURNAME_D:
                    return allUsers.OrderByDescending(x => x.Surname).ToList();
                case UsersSortOption.ROLE_A:
                    return allUsers.OrderBy(x => x._Role).ToList();
                case UsersSortOption.ROLE_D:
                    return allUsers.OrderByDescending(x => x._Role).ToList();
                default:
                    break;
            }
            return allUsers;
        }
        #endregion

        #region MyProfile
        public ActionResult MyProfile()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        break;
                    case Role.MANAGER:
                        return RedirectToAction("MyProfile", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("MyProfile", "Tourist");
                }
            }

            UserViewModel viewModel = new UserViewModel() { 
                User = (User)Session["user"]
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult MyProfileRequest()
        {
            Administrator a = AdministratorsSerializer.Read(((Administrator)Session["user"]).Username);
            a.Password = Request["password"];
            a.Name = Request["name"];
            a.Surname = Request["surname"];
            a.Date = Request["date"];
            a.Email = Request["email"];
            a._Gender = (Gender)Enum.Parse(typeof(Gender), Request["gender"].ToUpper());

            UserViewModel viewModel = new UserViewModel();

            // validation
            if (UserValidator.ValidateUser(a as User))
            {
                if (AdministratorsSerializer.Update(a, ((User)Session["user"]).Username))
                {
                    ViewBag.Success = true;
                    Session["user"] = a;
                    viewModel.User = a as User;
                    return View("MyProfile", viewModel);
                }                
            }

            ViewBag.Success = false;
            viewModel.User = (User)Session["user"];

            return View("MyProfile", viewModel);
        }

        #endregion

        #region Tourists
        public ActionResult Tourists()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        break;
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            List<Tourist> tourists = TouristsSerializer.Read();
            if (tourists.Count > 0)
            {
                tourists = tourists.FindAll(x => x.NumOfCancelings >= 2);
            }
            ViewBag.Users = tourists;
            return View();
        }

        [HttpPost]
        public ActionResult BlockTourist()
        {
            string username = Request["username"];
            try
            {
                Tourist t = TouristsSerializer.Read(Request["username"]);
                t.Blocked = !t.Blocked;
                TouristsSerializer.Update(t, t.Username);
            }
            catch (Exception)
            {
                ViewBag.message = "Error processing data";
            }
            return RedirectToAction("Tourists");
        }
        #endregion

        public bool AuthorizationCheck(User user)
        {
            if (user == null)
            {
                return false;
            }
            switch (user._Role)
            {
                case Role.ADMINISTRATOR:
                    return true;
                case Role.MANAGER:
                    return false;
                case Role.TOURIST:
                    return false;
                default:
                    return false;
            }
        }

        public ActionResult NavigateToUserController(User user)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            switch (user._Role)
            {
                case Role.ADMINISTRATOR:
                    return RedirectToAction("Home", "Admin");
                case Role.MANAGER:
                    return RedirectToAction("Home", "Manager");
                case Role.TOURIST:
                    return RedirectToAction("Home", "Tourist");
                default:
                    return RedirectToAction("Login", "UnregisteredUser");
            }
        }

        public List<Arrangement> ReadArrangements(string title) {

            switch (title)
            {
                case "Future Arrangements":
                    return ArrangementsSerializer.Read().FindAll(x =>
                        x.StringToDate(x.StartDate).Ticks > DateTime.Now.Ticks);
                case "Past And Current Arrangements":
                    return ArrangementsSerializer.Read().FindAll(x =>
                        x.StringToDate(x.StartDate).Ticks <= DateTime.Now.Ticks);
                case "All Arrangements":
                    return ArrangementsSerializer.Read();
                default:
                    return new List<Arrangement>();
            }
        }
    }
}