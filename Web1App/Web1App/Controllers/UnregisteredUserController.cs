﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Web1App.Models;
using Web1App.ViewModelComponents;
using Web1App.ViewModels;

namespace Web1App.Controllers
{
    public class UnregisteredUserController : Controller
    {
        #region HomePages
        public ActionResult Index()
        {
            if (Session["user"] != null)
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                Title = "Future Arrangements",
                ArrangementsSearch = new ArrangementsSearch(),
                Arrangements = Sort(ArrangementsSerializer.Read().FindAll
                    (x => x.StringToDate(x.StartDate).Ticks > DateTime.Now.Ticks),
                    new ArrangementsSort() { Option = ArrangementsSortOption.STARTDATE_A}),
                ArrangementsSort = new ArrangementsSort(),
            };
            return View(home);
        }

        public ActionResult PastAndCurrentArrangements()
        {
            if (Session["user"] != null)
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("PastAndCurrentArrangements", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("PastAndCurrentArrangements", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("PastAndCurrentArrangements", "Tourist");
                }
            }
            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                Title = "Past And Current Arrangements",
                ArrangementsSearch = new ArrangementsSearch(),
                Arrangements = Sort(ArrangementsSerializer.Read().FindAll
                    (x => x.StringToDate(x.StartDate).Ticks <= DateTime.Now.Ticks),
                    
                    new ArrangementsSort() { Option = ArrangementsSortOption.ENDDATE_D}),
                ArrangementsSort = new ArrangementsSort(),
            };
            return View(home);
        }

        public ActionResult AllArrangements()
        {
            if (Session["user"] != null)
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("AllArrangements", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("AllArrangements", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("AllArrangements", "Tourist");
                }
            }
            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                Title = "All Arrangements",
                ArrangementsSearch = new ArrangementsSearch(),
                Arrangements = ArrangementsSerializer.Read(),
                ArrangementsSort = new ArrangementsSort(),
            };
            return View(home);
        }

        [HttpPost]
        public ActionResult SearchSortFutureArrangements()
        {
            if (Session["user"] != null)
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("FutureArrangements", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("PastAndCurrentArrangements", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("AllArrangements", "Tourist");
                }
            }
            ArrangementsSearch arrangementSearch = new ArrangementsSearch()
            {
                StartDateFrom = Request["startDateFrom"],
                StartDateTo = Request["startDateTo"],
                EndDateFrom = Request["endDateFrom"],
                EndDateTo = Request["endDateTo"],
                Name = Request["name"],
                Transport = (Transport)Enum.Parse(typeof(Transport), Request["transport"].ToUpper()),
                ArrangementType = (ArrangementType)Enum.Parse(typeof(ArrangementType), Request["arrangementType"].ToUpper()),
            };
            ArrangementsSort arrangementSort = new ArrangementsSort
            {
                Option = (ArrangementsSortOption)Enum.Parse(typeof(ArrangementsSortOption), Request["arr_sort"].ToUpper()),
            };
            List<Arrangement> arrangements = ArrangementsSerializer.Read().FindAll(x =>
                    x.StringToDate(x.StartDate).Ticks > DateTime.Now.Ticks);
            if (Request["Search"] != null)
                arrangements = Search(arrangements, arrangementSearch);
            else
                arrangements = Sort(arrangements, arrangementSort);

            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                ArrangementsSearch = arrangementSearch,
                Arrangements = arrangements,
                Title = "Future Arrangements",
                ArrangementsSort = arrangementSort,
            };

            return View("Index", home);
        }

        [HttpPost]
        public ActionResult SearchSortPastAndCurrentArrangements()
        {
            if (Session["user"] != null)
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("FutureArrangements", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("PastAndCurrentArrangements", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("AllArrangements", "Tourist");
                }
            }
            ArrangementsSearch arrangementSearch = new ArrangementsSearch()
            {
                StartDateFrom = Request["startDateFrom"],
                StartDateTo = Request["startDateTo"],
                EndDateFrom = Request["endDateFrom"],
                EndDateTo = Request["endDateTo"],
                Name = Request["name"],
                Transport = (Transport)Enum.Parse(typeof(Transport), Request["transport"].ToUpper()),
                ArrangementType = (ArrangementType)Enum.Parse(typeof(ArrangementType), Request["arrangementType"].ToUpper()),
            };
            ArrangementsSort arrangementSort = new ArrangementsSort
            {
                Option = (ArrangementsSortOption)Enum.Parse(typeof(ArrangementsSortOption), Request["arr_sort"].ToUpper()),
            };
            List<Arrangement> arrangements = ArrangementsSerializer.Read().FindAll(x =>
                    x.StringToDate(x.StartDate).Ticks <= DateTime.Now.Ticks);
            if (Request["Search"] != null)
                arrangements = Search(arrangements, arrangementSearch);
            else
                arrangements = Sort(arrangements, arrangementSort);

            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                ArrangementsSearch = arrangementSearch,
                Arrangements = arrangements,
                Title = "Past And Current Arrangements",
                ArrangementsSort = arrangementSort,
            };
            return View("PastAndCurrentArrangements", home);
        }

        [HttpPost]
        public ActionResult SearchSortAllArrangements()
        {
            if (Session["user"] != null)
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("FutureArrangements", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("PastAndCurrentArrangements", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("AllArrangements", "Tourist");
                }
            }
            ArrangementsSearch arrangementSearch = new ArrangementsSearch()
            {
                StartDateFrom = Request["startDateFrom"],
                StartDateTo = Request["startDateTo"],
                EndDateFrom = Request["endDateFrom"],
                EndDateTo = Request["endDateTo"],
                Name = Request["name"],
                Transport = (Transport)Enum.Parse(typeof(Transport), Request["transport"].ToUpper()),
                ArrangementType = (ArrangementType)Enum.Parse(typeof(ArrangementType), Request["arrangementType"].ToUpper()),
            };
            ArrangementsSort arrangementSort = new ArrangementsSort
            {
                Option = (ArrangementsSortOption)Enum.Parse(typeof(ArrangementsSortOption), Request["arr_sort"].ToUpper()),
            };
            List<Arrangement> arrangements = ArrangementsSerializer.Read();
            if (Request["Search"] != null)
                arrangements = Search(arrangements, arrangementSearch);
            else
                arrangements = Sort(arrangements, arrangementSort);

            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                ArrangementsSearch = arrangementSearch,
                Arrangements = arrangements,
                Title = "All Arrangements",
                ArrangementsSort = arrangementSort,
            };

            return View("AllArrangements", home);
        }

        public List<Arrangement> Search(List<Arrangement> arrangements, ArrangementsSearch search)
        {
            if (search.StartDateFrom != null && search.StartDateFrom != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.StartDate).Ticks >= x.StringToDate(search.StartDateFrom).Ticks);
            }
            if (search.StartDateTo != null && search.StartDateTo != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.StartDate).Ticks <= x.StringToDate(search.StartDateTo).Ticks);
            }
            if (search.EndDateFrom != null && search.EndDateFrom != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.EndDate).Ticks >= x.StringToDate(search.EndDateFrom).Ticks);
            }
            if (search.EndDateTo != null && search.EndDateTo != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.EndDate).Ticks <= x.StringToDate(search.EndDateTo).Ticks);
            }
            if (search.Name != null)
            {
                arrangements = arrangements.FindAll(x => x.Name.ToUpper().Contains(search.Name.ToUpper()));
            }
            arrangements = arrangements.FindAll(x => x._ArrangementType == search.ArrangementType);
            arrangements = arrangements.FindAll(x => x._Transport == search.Transport);
            return arrangements;
        }

        public List<Arrangement> Sort(List<Arrangement> arrangements, ArrangementsSort arrangementsSort)
        {

            switch (arrangementsSort.Option)
            {
                case ArrangementsSortOption.NAME_A:
                    return arrangements.OrderBy(x => x.Name).ToList();
                case ArrangementsSortOption.NAME_D:
                    return arrangements.OrderByDescending(x => x.Name).ToList();
                case ArrangementsSortOption.STARTDATE_A:
                    return arrangements.OrderBy(x => x.StringToDate(x.StartDate)).ToList();
                case ArrangementsSortOption.STARTDATE_D:
                    return arrangements.OrderByDescending(x => x.StringToDate(x.StartDate)).ToList();
                case ArrangementsSortOption.ENDDATE_A:
                    return arrangements.OrderBy(x => x.StringToDate(x.EndDate)).ToList();
                case ArrangementsSortOption.ENDDATE_D:
                    return arrangements.OrderByDescending(x => x.StringToDate(x.EndDate)).ToList();
                default:
                    return new List<Arrangement>();
            }
        }
        #endregion

        #region Arrangement

        [HttpPost]
        public ActionResult SelectArrangement()
        {
            if (Session["user"] != null)
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            ArrangementViewModel arrangementViewModel = new ArrangementViewModel {
                Arrangement = ArrangementsSerializer.Read().Find(x => x.ID == Request["arrangement_id"]),
                AccommodationSearch = new AccommodationSearch(),
                AccommodationSort = new AccommodationSort()
            };
            return View("Arrangement", arrangementViewModel);
        }

        [HttpPost]
        public ActionResult SearchSortAccommodations()
        {
            if (Session["user"] != null)
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            ArrangementViewModel arrangementModelView = new ArrangementViewModel();
            arrangementModelView.Arrangement = ArrangementsSerializer.Read(Request["arrangement_id"]);
            if (Request["search"] != null)
                arrangementModelView.Arrangement._Accommodations = SearchAccommodations(arrangementModelView.Arrangement);
            else
                arrangementModelView.Arrangement._Accommodations = SortAccommodations(arrangementModelView.Arrangement);
            arrangementModelView.AccommodationSearch = new AccommodationSearch(
                (AccommodationType)Enum.Parse(typeof(AccommodationType), Request["acc_type"].ToUpper()),
                Request["acc_name"],
                Request["pool"] != null,
                Request["spa"] != null,
                Request["pwd"] != null,
                Request["wifi"] != null
                );
            arrangementModelView.AccommodationSort = new AccommodationSort(
                (AccommodationSortOption)Enum.Parse(typeof(AccommodationSortOption), Request["accommodation_sort"].ToUpper())
                );
            return View("Arrangement", arrangementModelView);
        }


        public List<Accommodation> SortAccommodations(Arrangement arrangement)
        {
            switch ((AccommodationSortOption)Enum.Parse(typeof(AccommodationSortOption), Request["accommodation_sort"].ToUpper()))
            {
                case AccommodationSortOption.NAME_A:
                    return arrangement._Accommodations.OrderBy(x => x.Name).ToList();
                case AccommodationSortOption.NAME_D:
                    return arrangement._Accommodations.OrderByDescending(x => x.Name).ToList();
                case AccommodationSortOption.UNITS_A:
                    return arrangement._Accommodations.OrderBy(x => x.TotalNumberOfUnits).ToList();
                case AccommodationSortOption.UNITS_D:
                    return arrangement._Accommodations.OrderByDescending(x => x.TotalNumberOfUnits).ToList();
                case AccommodationSortOption.FREE_UNITS_A:
                    return arrangement._Accommodations.OrderBy(x => x.NumberOfFreeUnits).ToList();
                case AccommodationSortOption.FREE_UNITS_D:
                    return arrangement._Accommodations.OrderByDescending(x => x.NumberOfFreeUnits).ToList();
                default:
                    return new List<Accommodation>();
            }
        }

        public List<Accommodation> SearchAccommodations(Arrangement arrangement)
        {
            List<Accommodation> accommodations = arrangement._Accommodations;
            accommodations = arrangement._Accommodations.FindAll
                (x => x._AccommodationType.ToString() == Request["acc_type"]);
            if (Request["acc_name"] != null)
            {
                accommodations = accommodations.FindAll(x => x.Name.ToUpper().Contains(Request["acc_name"].ToUpper()));
            }
            if (Request["pool"] != null)
            {
                accommodations = accommodations.FindAll(x => x.Pool);
            }
            if (Request["spa"] != null)
            {
                accommodations = accommodations.FindAll(x => x.Spa);
            }
            if (Request["wifi"] != null)
            {
                accommodations = accommodations.FindAll(x => x.WiFi);
            }
            if (Request["pwd"] != null)
            {
                accommodations = accommodations.FindAll(x => x.AdaptedForDisabledPeople);
            }
            return accommodations;
        }

        #endregion

        #region Accommodation

        [HttpPost]
        public ActionResult SelectAccommodation()
        {
            if (Session["user"] != null)
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            AccommodationViewModel viewModel = new AccommodationViewModel() {
                ArrangementId = Request["arrangement_id"],
                Accommodation = ArrangementsSerializer.Read(Request["arrangement_id"])._Accommodations.Find(x => x.ID == Request["accommodation_id"]),
                AccommodationUnitsSearch = new AccommodationUnitsSearch(),
                AccommodationUnitsSort = new AccommodationUnitsSort()
            };
            return View("Accommodation", viewModel);
        }

        [HttpPost]
        public ActionResult SearchSortAccommodationUnits() 
        {
            if (Session["user"] != null)
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            AccommodationViewModel viewModel = new AccommodationViewModel() { 
                Accommodation = ArrangementsSerializer.Read(Request["arrangement_id"])._Accommodations.Find(x => x.ID == Request["accommodation_id"]),
                ArrangementId = Request["arrangement_id"],
                AccommodationUnitsSearch = new AccommodationUnitsSearch()
                {
                    MinPrice = Convert.ToInt32(Request["minprice"]),
                    MaxPrice = Convert.ToInt32(Request["maxprice"]),
                    PetFree = Request["pets"] != null,
                    MinNumberOfGuests = Convert.ToInt32(Request["minNumOfGuests"]),
                    MaxNumberOfGuests = Convert.ToInt32(Request["maxNumOfGuests"]),
                },
                AccommodationUnitsSort = new AccommodationUnitsSort((AccommodationUnitsSortOption)Enum.Parse(typeof(AccommodationUnitsSortOption), Request["accommodationUnitsSort"].ToUpper()))               
            };
            if (Request["search"] != null)
                viewModel.Accommodation.Units = SearchAccommodationUnits(viewModel.Accommodation.Units, viewModel.AccommodationUnitsSearch);
            else
                viewModel.Accommodation.Units = SortAccommodationUnits(viewModel.Accommodation.Units, viewModel.AccommodationUnitsSort);
            return View("Accommodation", viewModel);
        }

        public List<AccommodationUnit> SearchAccommodationUnits(List<AccommodationUnit> units, AccommodationUnitsSearch search)
        {
            if (search.MinPrice != 0)
            {
                units = units.FindAll(x => x.Price >= search.MinPrice);
            }
            if (search.MaxPrice != 0)
            {
                units = units.FindAll(x => x.Price <= search.MaxPrice);
            }
            if (search.PetFree)
            {
                units = units.FindAll(x => x.Pets);
            }
            if (search.MinNumberOfGuests != 0)
            {
                units = units.FindAll(x => x.MaxNumOfGuests >= search.MinNumberOfGuests);
            }
            if (search.MaxNumberOfGuests != 0)
            {
                units = units.FindAll(x => x.MaxNumOfGuests <= search.MaxNumberOfGuests);
            }
            return units;
        }

        public List<AccommodationUnit> SortAccommodationUnits(List<AccommodationUnit> units, AccommodationUnitsSort sort)
        {
            switch (sort.Option)
            {
                case AccommodationUnitsSortOption.ANOG_A:
                    return units.OrderBy(x => x.MaxNumOfGuests).ToList();
                case AccommodationUnitsSortOption.ANOG_D:
                    return units.OrderByDescending(x => x.MaxNumOfGuests).ToList();
                case AccommodationUnitsSortOption.PRICE_A:
                    return units.OrderBy(x => x.Price).ToList();
                case AccommodationUnitsSortOption.PRICE_D:
                    return units.OrderByDescending(x => x.Price).ToList();
                default:
                    return units;
            }
        }


        #endregion

        #region AccommodationUnit

        [HttpPost]
        public ActionResult SelectAccommodationUnit()
        {
            if (Session["user"] != null)
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            AccommodationUnitViewModel viewModel = new AccommodationUnitViewModel() {
                ArrangementId = Request["arrangement_id"],
                AccommodationId = Request["accommodation_id"],
                AccommodationUnit = ArrangementsSerializer.Read
                (Request["arrangement_id"])._Accommodations.Find
                (x => x.ID == Request["accommodation_id"]).Units.Find
                (x => x.ID == Request["accommodation_unit_id"])
            };
            return View("AccommodationUnit", viewModel);
        }

        #endregion

        #region Login
        public ActionResult Login()
        {
            if (Session["user"] != null)
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult LoginRequest()
        {
            // Authentication
            User a = null;
            short i = 0;
            while (i < 3)
            {
                if (i == 0)
                    a = AdministratorsSerializer.Read(Request["username"]);
                if (i == 1)
                    a = ManagersSerializer.Read(Request["username"]);
                if (i == 2)
                    a = TouristsSerializer.Read(Request["username"]);
                if (a != null)
                    break;
                i++;
            }
            if (a == null)
            {
                ViewBag.LoginFailMessage = "Authentication failed: User not found";
                return View("Login");
            }
            if (!a.Password.Equals(Request["password"]))
            {
                ViewBag.LoginFailMessage = "Authentication failed: Incorrect password";
                return View("Login");
            }

            if (a._Role == Role.TOURIST)
            {
                Tourist t = TouristsSerializer.Read(Request["username"]);
                if (t.Blocked)
                {
                    ViewBag.LoginFailMessage = "This user is blocked by administrator";
                    return View("Login");
                }
            }

            // Set user
            Session["user"] = a;

            switch (a._Role)
            {
                case Role.ADMINISTRATOR:
                    return RedirectToAction("Home", "Admin");
                case Role.MANAGER:
                    return RedirectToAction("Home", "Manager");
                case Role.TOURIST:
                    return RedirectToAction("Home", "Tourist");
                default:
                    ViewBag.LoginFailMessage = "Redirection failed: Unknown user role";
                    return View("Login");
            }
        }

        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();

            return RedirectToAction("Login");
        }
        #endregion

        #region Registration

        public ActionResult Registration()
        {
            if (Session["user"] != null)
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            return View();
        }

        public ActionResult RegistrationRequest()
        {
            Tourist newTourist = new Tourist
                (Request["username"],
                Request["password"],
                Request["name"],
                Request["surname"],
                Request["email"],
                Request["date"],
                (Gender)Enum.Parse(typeof(Gender), Request["gender"].ToUpper()));

            var context = new ValidationContext(newTourist, null, null);
            if (Validator.TryValidateObject(newTourist, context, null))
            {
                if (UserValidator.ValidateUser(newTourist))
                {
                    TouristsSerializer.Create(newTourist);
                    Session["user"] = newTourist;
                    return RedirectToAction("Home", "Tourist");
                }
            }

            ViewBag.message = true;

            return View("Registration");
        }
        #endregion
    }
}