﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web1App.Models;
using Web1App.ViewModelComponents;
using Web1App.ViewModels;

namespace Web1App.Controllers
{
    public class ManagerController : Controller
    {
        #region HomePages
        public ActionResult Home()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                Title = "Future Arrangements",
                ArrangementsSearch = new ArrangementsSearch(),
                Arrangements = ArrangementsSerializer.Read().FindAll
                (x => x.StringToDate(x.StartDate).Ticks > DateTime.Now.Ticks),
                ArrangementsSort = new ArrangementsSort(),
            };

            ViewBag.message = false;

            return View(home);
        }

        public ActionResult PastAndCurrentArrangements()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                Title = "Past And Current Arrangements",
                ArrangementsSearch = new ArrangementsSearch(),
                Arrangements = ArrangementsSerializer.Read().FindAll
                (x => x.StringToDate(x.StartDate).Ticks <= DateTime.Now.Ticks),
                ArrangementsSort = new ArrangementsSort(),
            };
            return View(home);
        }

        public ActionResult AllArrangements()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                Title = "All Arrangements",
                ArrangementsSearch = new ArrangementsSearch(),
                Arrangements = ArrangementsSerializer.Read(),
                ArrangementsSort = new ArrangementsSort(),
            };
            return View(home);
        }

        [HttpPost]
        public ActionResult SearchSortFutureArrangements()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            ArrangementsSearch arrangementSearch = new ArrangementsSearch()
            {
                StartDateFrom = Request["startDateFrom"],
                StartDateTo = Request["startDateTo"],
                EndDateFrom = Request["endDateFrom"],
                EndDateTo = Request["endDateTo"],
                Name = Request["name"],
                Transport = (Transport)Enum.Parse(typeof(Transport), Request["transport"].ToUpper()),
                ArrangementType = (ArrangementType)Enum.Parse(typeof(ArrangementType), Request["arrangementType"].ToUpper()),
            };
            ArrangementsSort arrangementSort = new ArrangementsSort
            {
                Option = (ArrangementsSortOption)Enum.Parse(typeof(ArrangementsSortOption), Request["arr_sort"].ToUpper()),
            };
            List<Arrangement> arrangements = ArrangementsSerializer.Read().FindAll(x =>
                    x.StringToDate(x.StartDate).Ticks > DateTime.Now.Ticks);
            if (Request["Search"] != null)
                arrangements = Search(arrangements, arrangementSearch);
            else
                arrangements = Sort(arrangements, arrangementSort);

            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                ArrangementsSearch = arrangementSearch,
                Arrangements = arrangements,
                Title = "Future Arrangements",
                ArrangementsSort = arrangementSort,
            };

            return View("Home", home);
        }

        [HttpPost]
        public ActionResult SearchSortPastAndCurrentArrangements()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            ArrangementsSearch arrangementSearch = new ArrangementsSearch()
            {
                StartDateFrom = Request["startDateFrom"],
                StartDateTo = Request["startDateTo"],
                EndDateFrom = Request["endDateFrom"],
                EndDateTo = Request["endDateTo"],
                Name = Request["name"],
                Transport = (Transport)Enum.Parse(typeof(Transport), Request["transport"].ToUpper()),
                ArrangementType = (ArrangementType)Enum.Parse(typeof(ArrangementType), Request["arrangementType"].ToUpper()),
            };
            ArrangementsSort arrangementSort = new ArrangementsSort
            {
                Option = (ArrangementsSortOption)Enum.Parse(typeof(ArrangementsSortOption), Request["arr_sort"].ToUpper()),
            };
            List<Arrangement> arrangements = ArrangementsSerializer.Read().FindAll(x =>
                    x.StringToDate(x.StartDate).Ticks <= DateTime.Now.Ticks);
            if (Request["Search"] != null)
                arrangements = Search(arrangements, arrangementSearch);
            else
                arrangements = Sort(arrangements, arrangementSort);

            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                ArrangementsSearch = arrangementSearch,
                Arrangements = arrangements,
                Title = "Past And Current Arrangements",
                ArrangementsSort = arrangementSort,
            };
            return View("PastAndCurrentArrangements", home);
        }

        [HttpPost]
        public ActionResult SearchSortAllArrangements()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            ArrangementsSearch arrangementSearch = new ArrangementsSearch()
            {
                StartDateFrom = Request["startDateFrom"],
                StartDateTo = Request["startDateTo"],
                EndDateFrom = Request["endDateFrom"],
                EndDateTo = Request["endDateTo"],
                Name = Request["name"],
                Transport = (Transport)Enum.Parse(typeof(Transport), Request["transport"].ToUpper()),
                ArrangementType = (ArrangementType)Enum.Parse(typeof(ArrangementType), Request["arrangementType"].ToUpper()),
            };
            ArrangementsSort arrangementSort = new ArrangementsSort
            {
                Option = (ArrangementsSortOption)Enum.Parse(typeof(ArrangementsSortOption), Request["arr_sort"].ToUpper()),
            };
            List<Arrangement> arrangements = ArrangementsSerializer.Read();
            if (Request["Search"] != null)
                arrangements = Search(arrangements, arrangementSearch);
            else
                arrangements = Sort(arrangements, arrangementSort);

            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                ArrangementsSearch = arrangementSearch,
                Arrangements = arrangements,
                Title = "All Arrangements",
                ArrangementsSort = arrangementSort,
            };

            return View("AllArrangements", home);
        }

        public List<Arrangement> Search(List<Arrangement> arrangements, ArrangementsSearch search)
        {
            if (search.StartDateFrom != null && search.StartDateFrom != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.StartDate).Ticks >= x.StringToDate(search.StartDateFrom).Ticks);
            }
            if (search.StartDateTo != null && search.StartDateTo != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.StartDate).Ticks <= x.StringToDate(search.StartDateTo).Ticks);
            }
            if (search.EndDateFrom != null && search.EndDateFrom != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.EndDate).Ticks >= x.StringToDate(search.EndDateFrom).Ticks);
            }
            if (search.EndDateTo != null && search.EndDateTo != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.EndDate).Ticks <= x.StringToDate(search.EndDateTo).Ticks);
            }
            if (search.Name != null)
            {
                arrangements = arrangements.FindAll(x => x.Name.ToUpper().Contains(search.Name.ToUpper()));
            }
            arrangements = arrangements.FindAll(x => x._ArrangementType == search.ArrangementType);
            arrangements = arrangements.FindAll(x => x._Transport == search.Transport);
            return arrangements;
        }

        public List<Arrangement> Sort(List<Arrangement> arrangements, ArrangementsSort arrangementsSort)
        {

            switch (arrangementsSort.Option)
            {
                case ArrangementsSortOption.NAME_A:
                    return arrangements.OrderBy(x => x.Name).ToList();
                case ArrangementsSortOption.NAME_D:
                    return arrangements.OrderByDescending(x => x.Name).ToList();
                case ArrangementsSortOption.STARTDATE_A:
                    return arrangements.OrderBy(x => x.StringToDate(x.StartDate)).ToList();
                case ArrangementsSortOption.STARTDATE_D:
                    return arrangements.OrderByDescending(x => x.StringToDate(x.StartDate)).ToList();
                case ArrangementsSortOption.ENDDATE_A:
                    return arrangements.OrderBy(x => x.StringToDate(x.EndDate)).ToList();
                case ArrangementsSortOption.ENDDATE_D:
                    return arrangements.OrderByDescending(x => x.StringToDate(x.EndDate)).ToList();
                default:
                    return new List<Arrangement>();
            }
        }
        #endregion

        #region Arrangement
        [HttpPost]
        public ActionResult SelectArrangement()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            ArrangementViewModel viewModel = new ArrangementViewModel
            {
                Arrangement = ArrangementsSerializer.Read().Find(x => x.ID == Request["arrangement_id"]),
                AccommodationSearch = new AccommodationSearch(),
                AccommodationSort = new AccommodationSort()
            };

            ViewBag.edit = ((Manager)Session["user"]).ArrangementsIDs.Contains(viewModel.Arrangement.ID);

            ViewBag.message = false;

            return View("Arrangement", viewModel);
        } 
        [HttpPost]
        public ActionResult SearchSortAccommodations()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            ArrangementViewModel arrangementModelView = new ArrangementViewModel();
            arrangementModelView.Arrangement = ArrangementsSerializer.Read(Request["arrangement_id"]);
            if (Request["search"] != null)
                arrangementModelView.Arrangement._Accommodations = SearchAccommodations(arrangementModelView.Arrangement);
            else
                arrangementModelView.Arrangement._Accommodations = SortAccommodations(arrangementModelView.Arrangement);
            arrangementModelView.AccommodationSearch = new AccommodationSearch(
                (AccommodationType)Enum.Parse(typeof(AccommodationType), Request["acc_type"].ToUpper()),
                Request["acc_name"],
                Request["pool"] != null,
                Request["spa"] != null,
                Request["pwd"] != null,
                Request["wifi"] != null
            );
            arrangementModelView.AccommodationSort = new AccommodationSort(
                (AccommodationSortOption)Enum.Parse(typeof(AccommodationSortOption), Request["accommodation_sort"].ToUpper())
            );

            ViewBag.message = false;
            ViewBag.edit = ((Manager)Session["user"]).ArrangementsIDs.Contains(arrangementModelView.Arrangement.ID);

            return View("Arrangement", arrangementModelView);
        }
        public List<Accommodation> SortAccommodations(Arrangement arrangement)
        {
            switch ((AccommodationSortOption)Enum.Parse(typeof(AccommodationSortOption), Request["accommodation_sort"].ToUpper()))
            {
                case AccommodationSortOption.NAME_A:
                    return arrangement._Accommodations.OrderBy(x => x.Name).ToList();
                case AccommodationSortOption.NAME_D:
                    return arrangement._Accommodations.OrderByDescending(x => x.Name).ToList();
                case AccommodationSortOption.UNITS_A:
                    return arrangement._Accommodations.OrderBy(x => x.TotalNumberOfUnits).ToList();
                case AccommodationSortOption.UNITS_D:
                    return arrangement._Accommodations.OrderByDescending(x => x.TotalNumberOfUnits).ToList();
                case AccommodationSortOption.FREE_UNITS_A:
                    return arrangement._Accommodations.OrderBy(x => x.NumberOfFreeUnits).ToList();
                case AccommodationSortOption.FREE_UNITS_D:
                    return arrangement._Accommodations.OrderByDescending(x => x.NumberOfFreeUnits).ToList();
                default:
                    return new List<Accommodation>();
            }
        }
        public List<Accommodation> SearchAccommodations(Arrangement arrangement)
        {
            List<Accommodation> accommodations = arrangement._Accommodations;
            accommodations = arrangement._Accommodations.FindAll
                (x => x._AccommodationType.ToString() == Request["acc_type"]);
            if (Request["acc_name"] != null)
            {
                accommodations = accommodations.FindAll(x => x.Name.ToUpper().Contains(Request["acc_name"].ToUpper()));
            }
            if (Request["pool"] != null)
            {
                accommodations = accommodations.FindAll(x => x.Pool);
            }
            if (Request["spa"] != null)
            {
                accommodations = accommodations.FindAll(x => x.Spa);
            }
            if (Request["wifi"] != null)
            {
                accommodations = accommodations.FindAll(x => x.WiFi);
            }
            if (Request["pwd"] != null)
            {
                accommodations = accommodations.FindAll(x => x.AdaptedForDisabledPeople);
            }
            return accommodations;
        }
        [HttpPost]
        public ActionResult EditDeleteArrangement()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }

            ViewBag.message = false;

            if (Request["edit"] != null)
            {
                Arrangement a = ArrangementsSerializer.Read(Request["arrangement_id"]);
                return View("EditArrangement", a);
            }

            // find reservations for this arrangement
            List<Reservation> reservations = ReservationsSerializer.Read();
            reservations = reservations.FindAll
                (x => x.ArrangementID == Request["arrangement_id"] &&
                x._ReservationStatus == ReservationStatus.ACTIVE);

            if (reservations.Count == 0)
            {
                ArrangementsSerializer.Delete(Request["arrangement_id"]);
            }
            else
            {
                ArrangementViewModel viewModel = new ArrangementViewModel
                {
                    Arrangement = ArrangementsSerializer.Read().Find(x => x.ID == Request["arrangement_id"]),
                    AccommodationSearch = new AccommodationSearch(),
                    AccommodationSort = new AccommodationSort()
                };

                ViewBag.edit = ((Manager)Session["user"]).ArrangementsIDs.Contains(viewModel.Arrangement.ID);

                ViewBag.message = true;
                ViewBag.messageText = "This arrangement can not be deleted because it has active reservations.";

                return View("Arrangement", viewModel);
            }

            ViewBag.message = true;
            ViewBag.messageText = "Arrangement has been deleted successfully.";

            HomeArrangementsViewModel hvm = new HomeArrangementsViewModel
            {
                Title = "Future Arrangements",
                ArrangementsSearch = new ArrangementsSearch(),
                Arrangements = ArrangementsSerializer.Read().FindAll
               (x => x.StringToDate(x.StartDate).Ticks > DateTime.Now.Ticks),
                ArrangementsSort = new ArrangementsSort(),
            };

            return View("Home", hvm);
        }
        [HttpPost]
        public ActionResult UpdateArrangement()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }

            ViewBag.message = false;

            if (Request["cancel"] == null)
            {
                Arrangement a = ArrangementsSerializer.Read(Request["arrangement_id"]);
                a._PlaceOfDeparture = new PlaceOfDeparture();
                a._PlaceOfDeparture.Latitude = Convert.ToDouble(Request["latitude"]);
                a._PlaceOfDeparture.Longitude = Convert.ToDouble(Request["longitude"]);
                a._PlaceOfDeparture.Address =
                    Request["street"] + "|" + Request["number"] + "|" + Request["city"] + "|" + Request["postal"];
                a.Name = Request["name"];
                a._ArrangementType = (ArrangementType)Enum.Parse(typeof(ArrangementType), Request["arrangementType"].ToUpper());
                a._Transport = (Transport)Enum.Parse(typeof(Transport), Request["transport"].ToUpper()); ;
                a.Location = Request["location"];
                a.SetStartDate(Request["startDate"]);
                a.SetEndDate(Request["endDate"]);
                a.TimeOfDeparture = Request["timeOfDeparture"];
                a.MaxNumOfPassengers = Convert.ToInt32(Request["maxNum"]);
                a.ArrangementDescription = Request["arrangementDescription"];
                a.TravelDescription = Request["travelDescription"];
                var img = Request.Files["poster-path"];
                if (img != null && img.ContentLength > 0)
                {
                    string path = "/Images/Managers/" + img.FileName;
                    a.Poster = path;
                    string fullPath = HttpContext.Server.MapPath(path);
                    img.SaveAs(HttpContext.Server.MapPath(path));
                }

                // validation

                var context = new ValidationContext(a, null, null);

                ViewBag.message = true;
                if (Validator.TryValidateObject(a, context, null))
                {
                    if (ArrangementsSerializer.Update(a))
                        ViewBag.messageText = "Arrangement has been updated successfully.";
                    else
                        ViewBag.messageText = "Error: arrangement has not been updated.";
                }
                else
                {
                    ViewBag.messageText = "Error: Validation failed.";
                }
            }
            ArrangementViewModel arrangementViewModel = new ArrangementViewModel
            {
                Arrangement = ArrangementsSerializer.Read(Request["arrangement_id"]),
                AccommodationSearch = new AccommodationSearch(),
                AccommodationSort = new AccommodationSort()
            };

            ViewBag.edit = true;

            return View("Arrangement", arrangementViewModel);
        }

        [HttpPost]
        public ActionResult NewAccommodation()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            ViewBag.arrangement_id = Request["arrangement_id"];
            return View();
        }
        public ActionResult CreateAccommodation()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            Accommodation newAcc = new Accommodation(
                (AccommodationType)Enum.Parse(typeof(AccommodationType), Request["acc_type"].ToUpper()),
                Request["name"],
                Request["pool"] != null,
                Request["spa"] != null,
                Request["pwd"] != null,
                Request["wifi"] != null
            );

            // validation
            var context = new ValidationContext(newAcc, null, null);

            ViewBag.message = true;
            // write to DB
            if (Validator.TryValidateObject(newAcc, context, null))
            {
                if (ArrangementsSerializer.CreateAccommodation(newAcc, Request["arrangement_id"]))
                {
                    ViewBag.messageText = "Accommodation has been created successfully.";
                }
                else
                {
                    ViewBag.messageText = "Error: accommodation has not been created.";
                }
            }
            else
            {
                ViewBag.messageText = "Error: validation failed.";
            }

            ArrangementViewModel viewModel = new ArrangementViewModel
            {
                Arrangement = ArrangementsSerializer.Read().Find(x => x.ID == Request["arrangement_id"]),
                AccommodationSearch = new AccommodationSearch(),
                AccommodationSort = new AccommodationSort()
            };

            ViewBag.edit = ((Manager)Session["user"]).ArrangementsIDs.Contains(viewModel.Arrangement.ID);

            return View("Arrangement", viewModel);
        }

        #endregion

        #region Accommodation
        [HttpPost]
        public ActionResult SelectAccommodation()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            List<Arrangement> arrs = ArrangementsSerializer.Read();
            Arrangement arr = arrs.Find(x => x._Accommodations.Find(y => y.ID == Request["accommodation_id"]) != null);
            Accommodation acc = arr._Accommodations.Find(y => y.ID == Request["accommodation_id"]);
            AccommodationViewModel vm = new AccommodationViewModel() {
                Accommodation = acc,
                ArrangementId = arr.ID,
                AccommodationUnitsSearch = new AccommodationUnitsSearch(),
                AccommodationUnitsSort = new AccommodationUnitsSort()
            };

            ViewBag.edit = ((Manager)Session["user"]).ArrangementsIDs.Contains(arr.ID);

            return View("Accommodation", vm);
        }

        [HttpPost]
        public ActionResult UpdateAccommodation()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            if (Request["cancel"] == null)
            {
                Accommodation acc = ArrangementsSerializer.ReadAccommodation(Request["accommodation_id"]);
                acc._AccommodationType = (AccommodationType)Enum.Parse(typeof(AccommodationType), Request["acc_type"].ToUpper());
                acc.Name = Request["name"];
                acc.Pool = Request["pool"] != null;
                acc.Spa = Request["spa"] != null;
                acc.AdaptedForDisabledPeople = Request["pwd"] != null;
                acc.WiFi = Request["wifi"] != null;

                // validation
                var context = new ValidationContext(acc, null, null);

                ViewBag.message = true;
                // write to DB
                if (Validator.TryValidateObject(acc, context, null))
                {
                    if (ArrangementsSerializer.UpdateAccommodation(acc))
                    {
                        ViewBag.messageText = "Accommodation has been updated successfully.";
                    }
                    else
                    {
                        ViewBag.messageText = "Error: accommodation has not been updated.";
                    }
                }
                else
                {
                    ViewBag.messageText = "Error: validation failed.";
                }
            }

            ViewBag.edit = true;

            AccommodationViewModel vm = new AccommodationViewModel()
            {
                Accommodation = ArrangementsSerializer.ReadAccommodation(Request["accommodation_id"]),
                ArrangementId = Request["arrangement_id"],
                AccommodationUnitsSearch = new AccommodationUnitsSearch(),
                AccommodationUnitsSort = new AccommodationUnitsSort()
            };

            return View("Accommodation", vm);
        }

        [HttpPost]
        public ActionResult EditDeleteAccommodation()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            if (Request["edit"] != null)
            {
                Accommodation a = ArrangementsSerializer.ReadAccommodation(Request["accommodation_id"]);
                ViewBag.arrangement_id = Request["arrangement_id"];
                return View("EditAccommodation", a);
            }

            ViewBag.message = true;
            ViewBag.messageText = "Accommodation can not be deleted.";

            // find arrangement for this accommodation
            Arrangement arr = ArrangementsSerializer.Read(Request["arrangement_id"]);


            if (arr.StringToDate(arr.StartDate).Ticks < DateTime.Now.Ticks)
            {
                if(ArrangementsSerializer.DeleteAccommodation(Request["accommodation_id"]))
                    ViewBag.messageText = "Accommodation has been deleted successfully.";
                else
                    ViewBag.messageText = "Error: deletion failed.";
            }

            ArrangementViewModel avm = new ArrangementViewModel() {
                Arrangement = ArrangementsSerializer.Read(Request["arrangement_id"]),
                AccommodationSearch = new AccommodationSearch(),
                AccommodationSort = new AccommodationSort()
            };

            ViewBag.edit = true;

            return View("Arrangement", avm);
        }

        [HttpPost]
        public ActionResult SearchSortAccommodationUnits()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            AccommodationViewModel viewModel = new AccommodationViewModel()
            {
                Accommodation = ArrangementsSerializer.Read(Request["arrangement_id"])._Accommodations.Find(x => x.ID == Request["accommodation_id"]),
                ArrangementId = Request["arrangement_id"],
                AccommodationUnitsSearch = new AccommodationUnitsSearch()
                {
                    MinPrice = Convert.ToInt32(Request["minprice"]),
                    MaxPrice = Convert.ToInt32(Request["maxprice"]),
                    PetFree = Request["pets"] != null,
                    MinNumberOfGuests = Convert.ToInt32(Request["minNumOfGuests"]),
                    MaxNumberOfGuests = Convert.ToInt32(Request["maxNumOfGuests"]),
                },
                AccommodationUnitsSort = new AccommodationUnitsSort((AccommodationUnitsSortOption)Enum.Parse(typeof(AccommodationUnitsSortOption), Request["accommodationUnitsSort"].ToUpper()))
            };
            if (Request["search"] != null)
                viewModel.Accommodation.Units = SearchAccommodationUnits(viewModel.Accommodation.Units, viewModel.AccommodationUnitsSearch);
            else
                viewModel.Accommodation.Units = SortAccommodationUnits(viewModel.Accommodation.Units, viewModel.AccommodationUnitsSort);

            ViewBag.edit = ((Manager)Session["user"]).ArrangementsIDs.Contains(Request["arrangement_id"]);

            return View("Accommodation", viewModel);
        }

        public List<AccommodationUnit> SearchAccommodationUnits(List<AccommodationUnit> units, AccommodationUnitsSearch search)
        {
            if (search.MinNumberOfGuests != 0)
                units = units.FindAll(x => x.MaxNumOfGuests >= search.MinNumberOfGuests);

            if (search.MaxNumberOfGuests != 0)
                units = units.FindAll(x => x.MaxNumOfGuests <= search.MaxNumberOfGuests);

            if (search.PetFree)
                units = units.FindAll(x => x.Pets == search.PetFree);

            if (search.MinPrice != 0)
                units = units.FindAll(x => x.Price >= search.MinPrice);

            if (search.MaxPrice != 0)
                units = units.FindAll(x => x.Price <= search.MaxPrice);

            return units;
        }

        public List<AccommodationUnit> SortAccommodationUnits(List<AccommodationUnit> units, AccommodationUnitsSort sort)
        {
            switch (sort.Option)
            {
                case AccommodationUnitsSortOption.ANOG_A:
                    units = units.OrderBy(x => x.MaxNumOfGuests).ToList();
                    break;
                case AccommodationUnitsSortOption.ANOG_D:
                    units = units.OrderBy(x => x.Price).ToList();
                    break;
                case AccommodationUnitsSortOption.PRICE_A:
                    units = units.OrderByDescending(x => x.MaxNumOfGuests).ToList();
                    break;
                case AccommodationUnitsSortOption.PRICE_D:
                    units = units.OrderByDescending(x => x.Price).ToList();
                    break;
                default:
                    break;
            }

            return units;
        }

        #endregion

        #region AccommodationUnit

        public ActionResult NewAccommodationUnit()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }

            ViewBag.arrangement_id = Request["arrangement_id"];
            ViewBag.accommodation_id = Request["accommodation_id"];

            return View();
        }

        [HttpPost]
        public ActionResult SelectAccommodationUnit()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }

            AccommodationUnit au = ArrangementsSerializer.ReadAccommodationUnit(Request["accommodation_unit_id"]);

            ViewBag.edit = ((Manager)Session["user"]).ArrangementsIDs.Contains(Request["arrangement_id"]);
            ViewBag.arrangement_id = Request["arrangement_id"];
            ViewBag.accommodation_id = Request["accommodation_id"];

            return View("AccommodationUnit", au);
        }

        [HttpPost]
        public ActionResult EditDeleteAccommodationUnit()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            if (Request["edit"] != null)
            {
                AccommodationUnit a = ArrangementsSerializer.ReadAccommodationUnit(Request["accommodation_unit_id"]);

                return View("EditAccommodationUnit", a);
            }

            ViewBag.message = true;

            List<Reservation> reservations = ReservationsSerializer.Read();
            if (reservations.Find(x => x.AccommodationUnitID == Request["accommodation_unit_id"]
                && x._ReservationStatus == ReservationStatus.ACTIVE) != null)
            {
                ViewBag.messageText = "Accommodation unit can not be deleted.";
            }
            else
            {
                if (ArrangementsSerializer.DeleteAccommodationUnit(Request["accommodation_unit_id"]))
                {
                    Accommodation acc = ArrangementsSerializer.ReadAccommodation(Request["accommodation_id"]);
                    acc.TotalNumberOfUnits--;
                    acc.NumberOfFreeUnits--;
                    ArrangementsSerializer.UpdateAccommodation(acc);
                    ViewBag.messageText = "Accommodation unit has been deleted successfully.";
                }
                else
                {
                    ViewBag.messageText = "Deletion failed.";
                }
            }

            string arrangement_id = "";
            string accommodation_id = "";
            List<Arrangement> arrangements = ArrangementsSerializer.Read(); 
            foreach (var a in arrangements)
            {
                foreach (var acc in a._Accommodations)
                {
                    foreach (var u in acc.Units)
                    {
                        if(u.ID == Request["accommodation_unit_id"])
                        {
                            arrangement_id = a.ID;
                            accommodation_id = acc.ID;
                            break;
                        }
                    }
                }
            }

            ViewBag.edit = true;

            AccommodationViewModel vm = new AccommodationViewModel()
            {
                Accommodation = ArrangementsSerializer.ReadAccommodation(accommodation_id),
                ArrangementId = arrangement_id,
                AccommodationUnitsSearch = new AccommodationUnitsSearch(),
                AccommodationUnitsSort = new AccommodationUnitsSort()
            };

            return View("Accommodation", vm);
        }

        public ActionResult UpdateAccommodationUnit()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            return View("AccommodationUnit");
        }


        public ActionResult CreateNewAccommodationUnit()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }

            ViewBag.message = true;
            ViewBag.arrangement_id = Request["arrangement_id"];
            ViewBag.accommodation_id = Request["accommodation_id"];

            AccommodationUnit au = new AccommodationUnit() { 
                Name = Request["name"],
                Price = Convert.ToDouble(Request["price"]),
                Pets = Request["pets"] != null,
                MaxNumOfGuests = Convert.ToInt32(Request["maxNum"]),
            };

            var context = new ValidationContext(au, null, null);

            if (Validator.TryValidateObject(au,context, null))
            {
                if (ArrangementsSerializer.CreateAccommodationUnit(au, Request["arrangement_id"], Request["accommodation_id"]))
                {
                    Accommodation acc = ArrangementsSerializer.ReadAccommodation(Request["accommodation_id"]);
                    acc.TotalNumberOfUnits++;
                    acc.NumberOfFreeUnits++;
                    ArrangementsSerializer.UpdateAccommodation(acc);
                    ViewBag.messageText = "New accommodation unit has been created successfully.";
                }
                else
                {
                    ViewBag.messageText = "Creation failed.";
                }
            }
            else
            {
                ViewBag.messageText = "Invalid data.";
            }

            ViewBag.edit = true;

            return View("NewAccommodationUnit");
        }

       


        #endregion

        #region Reservations

        public ActionResult Reservations()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            return View();
        }

        #endregion

        #region MyProfile
        public ActionResult MyProfile()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("MyProfile", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("MyProfile", "Tourist");
                }
            }

            UserViewModel viewModel = new UserViewModel()
            {
                User = (User)Session["user"]
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult MyProfileRequest()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("MyProfile", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("MyProfile", "Tourist");
                }
            }
            string username = ((Manager)Session["user"]).Username;
            Manager m = ManagersSerializer.Read(username);
            m.Password = Request["password"];
            m.Name = Request["name"];
            m.Surname = Request["surname"];
            m.Date = Request["date"];
            m.Email = Request["email"];
            m._Gender = (Gender)Enum.Parse(typeof(Gender), Request["gender"].ToUpper());

            UserViewModel viewModel = new UserViewModel();

            // validation
            if (UserValidator.ValidateUser(m as User))
            {
                if (ManagersSerializer.Update(m, username))
                {
                    ViewBag.Success = true;
                    Session["user"] = m;
                    viewModel.User = m as User;
                    return View("MyProfile", viewModel);
                }
            }

            ViewBag.Success = false;
            viewModel.User = (User)Session["user"];

            return View("MyProfile", viewModel);
        }

        #endregion

        #region MyArrangements
        
        public ActionResult MyArrangements()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            Manager curr = (Manager)Session["user"];
            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                Title = "My Arrangements",
                ArrangementsSearch = new ArrangementsSearch(),
                Arrangements = ArrangementsSerializer.Read().FindAll(x => curr.ArrangementsIDs.Contains(x.ID)),
                ArrangementsSort = new ArrangementsSort(),
            };

            ViewBag.message = false;

            return View(home);
        }
        
        [HttpPost]
        public ActionResult SearchSortMyArrangements()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            ArrangementsSearch arrangementSearch = new ArrangementsSearch()
            {
                StartDateFrom = Request["startDateFrom"],
                StartDateTo = Request["startDateTo"],
                EndDateFrom = Request["endDateFrom"],
                EndDateTo = Request["endDateTo"],
                Name = Request["name"],
                Transport = (Transport)Enum.Parse(typeof(Transport), Request["transport"].ToUpper()),
                ArrangementType = (ArrangementType)Enum.Parse(typeof(ArrangementType), Request["arrangementType"].ToUpper()),
            };
            ArrangementsSort arrangementSort = new ArrangementsSort
            {
                Option = (ArrangementsSortOption)Enum.Parse(typeof(ArrangementsSortOption), Request["arr_sort"].ToUpper()),
            };
            Manager curr = (Manager)Session["user"];
            List<Arrangement> arrangements = ArrangementsSerializer.Read().FindAll(x => curr.ArrangementsIDs.Contains(x.ID));
            if (Request["Search"] != null)
                arrangements = Search(arrangements, arrangementSearch);
            else
                arrangements = Sort(arrangements, arrangementSort);

            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                ArrangementsSearch = arrangementSearch,
                Arrangements = arrangements,
                Title = "My Arrangements",
                ArrangementsSort = arrangementSort,
            };

            ViewBag.message = false;

            return View("MyArrangements", home);
        }

        
        #endregion

        #region NewArrangement
        public ActionResult NewArrangement()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            ViewBag.message = false;
            return View();
        }

        [HttpPost]
        public ActionResult Create()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        break;
                    case Role.TOURIST:
                        return RedirectToAction("Home", "Tourist");
                }
            }
            Arrangement a = new Arrangement();
            a._PlaceOfDeparture = new PlaceOfDeparture();
            a._PlaceOfDeparture.Latitude = Convert.ToDouble(Request["latitude"]);
            a._PlaceOfDeparture.Longitude = Convert.ToDouble(Request["longitude"]);
            a._PlaceOfDeparture.Address =
                Request["street"] + "|" + Request["number"] + "|" + Request["city"] + "|" + Request["postal"];
            a.Name = Request["name"];
            a._ArrangementType = (ArrangementType)Enum.Parse(typeof(ArrangementType), Request["arranType"].ToUpper());
            a._Transport = (Transport)Enum.Parse(typeof(Transport), Request["transport"].ToUpper()); ;
            a.Location = Request["location"];
            a.SetStartDate(Request["startDate"]);
            a.SetEndDate(Request["endDate"]);
            a.TimeOfDeparture = Request["timeOfDeparture"];
            a.MaxNumOfPassengers = Convert.ToInt32(Request["maxNum"]);
            a.ArrangementDescription = Request["arrangDescription"];
            a.TravelDescription = Request["travelDescription"];
            var img = Request.Files["poster-path"];
            if (img != null && img.ContentLength > 0)
            {
                string path = "/Images/Managers/" + img.FileName;
                a.Poster = path;
                string fullPath = HttpContext.Server.MapPath(path);
                img.SaveAs(HttpContext.Server.MapPath(path));
            }

            // validation

            var context = new ValidationContext(a, null, null);

            ViewBag.message = true;
            if (Validator.TryValidateObject(a, context, null))
            {
                if (ArrangementsSerializer.Create(a))
                {
                    Manager curr = (Manager)Session["user"];
                    curr.ArrangementsIDs.Add(a.ID);
                    ManagersSerializer.Update(curr, curr.Username);
                    Session["user"] = curr;
                    ViewBag.messageText = "Arrangement has been created successfully.";
                    ArrangementViewModel arrangementViewModel = new ArrangementViewModel
                    {
                        Arrangement = a,
                        AccommodationSearch = new AccommodationSearch(),
                        AccommodationSort = new AccommodationSort()
                    };
                    ViewBag.edit = true;
                    return View("Arrangement", arrangementViewModel);
                }
                else {
                    ViewBag.messageText = "Error: arrangement has not been created.";
                }
            }
            else
            {
                ViewBag.messageText = "Error: Validation failed.";
            }

            return View("NewArrangement");
        }
        #endregion

        public bool AuthorizationCheck(User user) {
            if (user == null)
            {
                return false;
            }
            switch (user._Role)
            {
                case Role.ADMINISTRATOR:
                    return false;
                case Role.MANAGER:
                    return true;
                case Role.TOURIST:
                    return false;
                default:
                    return false;
            }
        }

        public ActionResult NavigateToUserController(User user) {
            if (user == null)
            {
                return RedirectToAction("UnregisteredUser","Login");
            }
            switch (user._Role)
            {
                case Role.ADMINISTRATOR:
                    return RedirectToAction("Home", "Admin");
                case Role.MANAGER:
                    return RedirectToAction("Home", "Manager");
                case Role.TOURIST:
                    return RedirectToAction("Home", "Tourist");
                default:
                    return RedirectToAction("UnregisteredUser", "Login");
            }
        }

    }
}