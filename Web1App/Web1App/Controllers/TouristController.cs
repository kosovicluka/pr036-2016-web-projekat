﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web1App.Models;
using Web1App.ViewModelComponents;
using Web1App.ViewModels;

namespace Web1App.Controllers
{
    public class TouristController : Controller
    {
        #region HomePages
        public ActionResult Home()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }
            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                Title = "Future Arrangements",
                ArrangementsSearch = new ArrangementsSearch(),
                Arrangements = ArrangementsSerializer.Read().FindAll
                (x => x.StringToDate(x.StartDate).Ticks > DateTime.Now.Ticks),
                ArrangementsSort = new ArrangementsSort(),
            };
            return View(home);
        }

        public ActionResult PastAndCurrentArrangements()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("PastAndCurrentArrangements", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("PastAndCurrentArrangements", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }
            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                Title = "Past And Current Arrangements",
                ArrangementsSearch = new ArrangementsSearch(),
                Arrangements = ArrangementsSerializer.Read().FindAll
                (x => x.StringToDate(x.StartDate).Ticks <= DateTime.Now.Ticks),
                ArrangementsSort = new ArrangementsSort(),
            };
            return View(home);
        }

        public ActionResult AllArrangements()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("AllArrangements", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("AllArrangements", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }
            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                Title = "All Arrangements",
                ArrangementsSearch = new ArrangementsSearch(),
                Arrangements = ArrangementsSerializer.Read(),
                ArrangementsSort = new ArrangementsSort(),
            };
            return View(home);
        }

        [HttpPost]
        public ActionResult SearchSortFutureArrangements()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }
            ArrangementsSearch arrangementSearch = new ArrangementsSearch()
            {
                StartDateFrom = Request["startDateFrom"],
                StartDateTo = Request["startDateTo"],
                EndDateFrom = Request["endDateFrom"],
                EndDateTo = Request["endDateTo"],
                Name = Request["name"],
                Transport = (Transport)Enum.Parse(typeof(Transport), Request["transport"].ToUpper()),
                ArrangementType = (ArrangementType)Enum.Parse(typeof(ArrangementType), Request["arrangementType"].ToUpper()),
            };
            ArrangementsSort arrangementSort = new ArrangementsSort
            {
                Option = (ArrangementsSortOption)Enum.Parse(typeof(ArrangementsSortOption), Request["arr_sort"].ToUpper()),
            };
            List<Arrangement> arrangements = ArrangementsSerializer.Read().FindAll(x =>
                    x.StringToDate(x.StartDate).Ticks > DateTime.Now.Ticks);
            if (Request["Search"] != null)
                arrangements = Search(arrangements, arrangementSearch);
            else
                arrangements = Sort(arrangements, arrangementSort);

            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                ArrangementsSearch = arrangementSearch,
                Arrangements = arrangements,
                Title = "Future Arrangements",
                ArrangementsSort = arrangementSort,
            };

            return View("Home", home);
        }

        [HttpPost]
        public ActionResult SearchSortPastAndCurrentArrangements()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("PastAndCurrentArrangements", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("PastAndCurrentArrangements", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }
            ArrangementsSearch arrangementSearch = new ArrangementsSearch()
            {
                StartDateFrom = Request["startDateFrom"],
                StartDateTo = Request["startDateTo"],
                EndDateFrom = Request["endDateFrom"],
                EndDateTo = Request["endDateTo"],
                Name = Request["name"],
                Transport = (Transport)Enum.Parse(typeof(Transport), Request["transport"].ToUpper()),
                ArrangementType = (ArrangementType)Enum.Parse(typeof(ArrangementType), Request["arrangementType"].ToUpper()),
            };
            ArrangementsSort arrangementSort = new ArrangementsSort
            {
                Option = (ArrangementsSortOption)Enum.Parse(typeof(ArrangementsSortOption), Request["arr_sort"].ToUpper()),
            };
            List<Arrangement> arrangements = ArrangementsSerializer.Read().FindAll(x =>
                    x.StringToDate(x.StartDate).Ticks <= DateTime.Now.Ticks);
            if (Request["Search"] != null)
                arrangements = Search(arrangements, arrangementSearch);
            else
                arrangements = Sort(arrangements, arrangementSort);

            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                ArrangementsSearch = arrangementSearch,
                Arrangements = arrangements,
                Title = "Past And Current Arrangements",
                ArrangementsSort = arrangementSort,
            };
            return View("PastAndCurrentArrangements", home);
        }

        [HttpPost]
        public ActionResult SearchSortAllArrangements()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("AllArrangements", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("AllArrangements", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }
            ArrangementsSearch arrangementSearch = new ArrangementsSearch()
            {
                StartDateFrom = Request["startDateFrom"],
                StartDateTo = Request["startDateTo"],
                EndDateFrom = Request["endDateFrom"],
                EndDateTo = Request["endDateTo"],
                Name = Request["name"],
                Transport = (Transport)Enum.Parse(typeof(Transport), Request["transport"].ToUpper()),
                ArrangementType = (ArrangementType)Enum.Parse(typeof(ArrangementType), Request["arrangementType"].ToUpper()),
            };
            ArrangementsSort arrangementSort = new ArrangementsSort
            {
                Option = (ArrangementsSortOption)Enum.Parse(typeof(ArrangementsSortOption), Request["arr_sort"].ToUpper()),
            };
            List<Arrangement> arrangements = ArrangementsSerializer.Read();
            if (Request["Search"] != null)
                arrangements = Search(arrangements, arrangementSearch);
            else
                arrangements = Sort(arrangements, arrangementSort);

            HomeArrangementsViewModel home = new HomeArrangementsViewModel
            {
                ArrangementsSearch = arrangementSearch,
                Arrangements = arrangements,
                Title = "All Arrangements",
                ArrangementsSort = arrangementSort,
            };

            return View("AllArrangements", home);
        }

        public List<Arrangement> Search(List<Arrangement> arrangements, ArrangementsSearch search)
        {
            if (search.StartDateFrom != null && search.StartDateFrom != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.StartDate).Ticks >= x.StringToDate(search.StartDateFrom).Ticks);
            }
            if (search.StartDateTo != null && search.StartDateTo != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.StartDate).Ticks <= x.StringToDate(search.StartDateTo).Ticks);
            }
            if (search.EndDateFrom != null && search.EndDateFrom != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.EndDate).Ticks >= x.StringToDate(search.EndDateFrom).Ticks);
            }
            if (search.EndDateTo != null && search.EndDateTo != "")
            {
                arrangements = arrangements.FindAll(x => x.StringToDate(x.EndDate).Ticks <= x.StringToDate(search.EndDateTo).Ticks);
            }
            if (search.Name != null)
            {
                arrangements = arrangements.FindAll(x => x.Name.ToUpper().Contains(search.Name.ToUpper()));
            }
            arrangements = arrangements.FindAll(x => x._ArrangementType == search.ArrangementType);
            arrangements = arrangements.FindAll(x => x._Transport == search.Transport);
            return arrangements;
        }

        public List<Arrangement> Sort(List<Arrangement> arrangements, ArrangementsSort arrangementsSort)
        {

            switch (arrangementsSort.Option)
            {
                case ArrangementsSortOption.NAME_A:
                    return arrangements.OrderBy(x => x.Name).ToList();
                case ArrangementsSortOption.NAME_D:
                    return arrangements.OrderByDescending(x => x.Name).ToList();
                case ArrangementsSortOption.STARTDATE_A:
                    return arrangements.OrderBy(x => x.StringToDate(x.StartDate)).ToList();
                case ArrangementsSortOption.STARTDATE_D:
                    return arrangements.OrderByDescending(x => x.StringToDate(x.StartDate)).ToList();
                case ArrangementsSortOption.ENDDATE_A:
                    return arrangements.OrderBy(x => x.StringToDate(x.EndDate)).ToList();
                case ArrangementsSortOption.ENDDATE_D:
                    return arrangements.OrderByDescending(x => x.StringToDate(x.EndDate)).ToList();
                default:
                    return new List<Arrangement>();
            }
        }
        #endregion

        #region Arrangement

        [HttpPost]
        public ActionResult SelectArrangement()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }
            ArrangementViewModel arrangementViewModel = new ArrangementViewModel
            {
                Arrangement = ArrangementsSerializer.Read().Find(x => x.ID == Request["arrangement_id"]),
                AccommodationSearch = new AccommodationSearch(),
                AccommodationSort = new AccommodationSort()
            };
            return View("Arrangement", arrangementViewModel);
        }

        [HttpPost]
        public ActionResult SearchSortAccommodations()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }
            ArrangementViewModel arrangementModelView = new ArrangementViewModel();
            arrangementModelView.Arrangement = ArrangementsSerializer.Read(Request["arrangement_id"]);
            if (Request["search"] != null)
                arrangementModelView.Arrangement._Accommodations = SearchAccommodations(arrangementModelView.Arrangement);
            else
                arrangementModelView.Arrangement._Accommodations = SortAccommodations(arrangementModelView.Arrangement);
            arrangementModelView.AccommodationSearch = new AccommodationSearch(
                (AccommodationType)Enum.Parse(typeof(AccommodationType), Request["acc_type"].ToUpper()),
                Request["acc_name"],
                Request["pool"] != null,
                Request["spa"] != null,
                Request["pwd"] != null,
                Request["wifi"] != null
                );
            arrangementModelView.AccommodationSort = new AccommodationSort(
                (AccommodationSortOption)Enum.Parse(typeof(AccommodationSortOption), Request["accommodation_sort"].ToUpper())
                );
            return View("Arrangement", arrangementModelView);
        }


        public List<Accommodation> SortAccommodations(Arrangement arrangement)
        {
            switch ((AccommodationSortOption)Enum.Parse(typeof(AccommodationSortOption), Request["accommodation_sort"].ToUpper()))
            {
                case AccommodationSortOption.NAME_A:
                    return arrangement._Accommodations.OrderBy(x => x.Name).ToList();
                case AccommodationSortOption.NAME_D:
                    return arrangement._Accommodations.OrderByDescending(x => x.Name).ToList();
                case AccommodationSortOption.UNITS_A:
                    return arrangement._Accommodations.OrderBy(x => x.TotalNumberOfUnits).ToList();
                case AccommodationSortOption.UNITS_D:
                    return arrangement._Accommodations.OrderByDescending(x => x.TotalNumberOfUnits).ToList();
                case AccommodationSortOption.FREE_UNITS_A:
                    return arrangement._Accommodations.OrderBy(x => x.NumberOfFreeUnits).ToList();
                case AccommodationSortOption.FREE_UNITS_D:
                    return arrangement._Accommodations.OrderByDescending(x => x.NumberOfFreeUnits).ToList();
                default:
                    return new List<Accommodation>();
            }
        }

        public List<Accommodation> SearchAccommodations(Arrangement arrangement)
        {
            List<Accommodation> accommodations = arrangement._Accommodations;
            accommodations = arrangement._Accommodations.FindAll
                (x => x._AccommodationType.ToString() == Request["acc_type"]);
            if (Request["acc_name"] != null)
            {
                accommodations = accommodations.FindAll(x => x.Name.ToUpper().Contains(Request["acc_name"].ToUpper()));
            }
            if (Request["pool"] != null)
            {
                accommodations = accommodations.FindAll(x => x.Pool);
            }
            if (Request["spa"] != null)
            {
                accommodations = accommodations.FindAll(x => x.Spa);
            }
            if (Request["wifi"] != null)
            {
                accommodations = accommodations.FindAll(x => x.WiFi);
            }
            if (Request["pwd"] != null)
            {
                accommodations = accommodations.FindAll(x => x.AdaptedForDisabledPeople);
            }
            return accommodations;
        }

        #endregion

        #region Accommodation

        [HttpPost]
        public ActionResult SelectAccommodation()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }
            AccommodationViewModel viewModel = new AccommodationViewModel()
            {
                ArrangementId = Request["arrangement_id"],
                Accommodation = ArrangementsSerializer.Read(Request["arrangement_id"])._Accommodations.Find(x => x.ID == Request["accommodation_id"]),
                AccommodationUnitsSearch = new AccommodationUnitsSearch(),
                AccommodationUnitsSort = new AccommodationUnitsSort()
            };
            return View("Accommodation", viewModel);
        }

        [HttpPost]
        public ActionResult SearchSortAccommodationUnits()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }
            AccommodationViewModel viewModel = new AccommodationViewModel()
            {
                Accommodation = ArrangementsSerializer.Read(Request["arrangement_id"])._Accommodations.Find(x => x.ID == Request["accommodation_id"]),
                ArrangementId = Request["arrangement_id"],
                AccommodationUnitsSearch = new AccommodationUnitsSearch()
                {
                    MinPrice = Convert.ToInt32(Request["minprice"]),
                    MaxPrice = Convert.ToInt32(Request["maxprice"]),
                    PetFree = Request["pets"] != null,
                    MinNumberOfGuests = Convert.ToInt32(Request["minNumOfGuests"]),
                    MaxNumberOfGuests = Convert.ToInt32(Request["maxNumOfGuests"]),
                },
                AccommodationUnitsSort = new AccommodationUnitsSort((AccommodationUnitsSortOption)Enum.Parse(typeof(AccommodationUnitsSortOption), Request["accommodationUnitsSort"].ToUpper()))
            };
            if (Request["search"] != null)
                viewModel.Accommodation.Units = SearchAccommodationUnits(viewModel.Accommodation.Units, viewModel.AccommodationUnitsSearch);
            else
                viewModel.Accommodation.Units = SortAccommodationUnits(viewModel.Accommodation.Units, viewModel.AccommodationUnitsSort);
            return View("Accommodation", viewModel);
        }

        public List<AccommodationUnit> SearchAccommodationUnits(List<AccommodationUnit> units, AccommodationUnitsSearch search)
        {
            if (search.MinPrice != 0)
            {
                units = units.FindAll(x => x.Price >= search.MinPrice);
            }
            if (search.MaxPrice != 0)
            {
                units = units.FindAll(x => x.Price <= search.MaxPrice);
            }
            if (search.PetFree)
            {
                units = units.FindAll(x => x.Pets);
            }
            if (search.MinNumberOfGuests != 0)
            {
                units = units.FindAll(x => x.MaxNumOfGuests >= search.MinNumberOfGuests);
            }
            if (search.MaxNumberOfGuests != 0)
            {
                units = units.FindAll(x => x.MaxNumOfGuests <= search.MaxNumberOfGuests);
            }
            return units;
        }

        public List<AccommodationUnit> SortAccommodationUnits(List<AccommodationUnit> units, AccommodationUnitsSort sort)
        {
            switch (sort.Option)
            {
                case AccommodationUnitsSortOption.ANOG_A:
                    return units.OrderBy(x => x.MaxNumOfGuests).ToList();
                case AccommodationUnitsSortOption.ANOG_D:
                    return units.OrderByDescending(x => x.MaxNumOfGuests).ToList();
                case AccommodationUnitsSortOption.PRICE_A:
                    return units.OrderBy(x => x.Price).ToList();
                case AccommodationUnitsSortOption.PRICE_D:
                    return units.OrderByDescending(x => x.Price).ToList();
                default:
                    return units;
            }
        }


        #endregion

        #region AccommodationUnit

        [HttpPost]
        public ActionResult SelectAccommodationUnit()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }
            AccommodationUnitViewModel viewModel = new AccommodationUnitViewModel()
            {
                ArrangementId = Request["arrangement_id"],
                AccommodationId = Request["accommodation_id"],
                AccommodationUnit = ArrangementsSerializer.Read
                (Request["arrangement_id"])._Accommodations.Find
                (x => x.ID == Request["accommodation_id"]).Units.Find
                (x => x.ID == Request["accommodation_unit_id"])
            };
            return View("AccommodationUnit", viewModel);
        }

        #endregion

        #region MyProfile
        public ActionResult MyProfile()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("MyProfile", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("MyProfile", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }

            UserViewModel viewModel = new UserViewModel()
            {
                User = (User)Session["user"]
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult MyProfileRequest()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }
            string username = ((Tourist)Session["user"]).Username;
            Tourist t = TouristsSerializer.Read(username);
            t.Password = Request["password"];
            t.Name = Request["name"];
            t.Surname = Request["surname"];
            t.Date = Request["date"];
            t.Email = Request["email"];
            t._Gender = (Gender)Enum.Parse(typeof(Gender), Request["gender"].ToUpper());

            UserViewModel viewModel = new UserViewModel();

            // validation
            if (UserValidator.ValidateUser(t as User))
            {
                if (TouristsSerializer.Update(t, username))
                {
                    ViewBag.Success = true;
                    Session["user"] = t;
                    viewModel.User = t as User;
                    return View("MyProfile", viewModel);
                }
            }

            ViewBag.Success = false;
            viewModel.User = (User)Session["user"];

            return View("MyProfile", viewModel);
        }
        #endregion

        #region Reservations
        public ActionResult Reservations()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }

            List<Reservation> userReservations = ReservationsSerializer.Read()
                .FindAll(x => x.TouristID == ((User)Session["user"]).Username);

            List<Arrangement> arrangements = ArrangementsSerializer.Read();

            foreach (var r in userReservations)
            {
                r.SetTourist((Tourist)Session["user"]);
                Arrangement a = null;
                if ((a = arrangements.Find(x => x.ID == r.ArrangementID)) != null)
                {
                    r.SetArrangement(a);
                    Accommodation acc = null;
                    if ((acc = a._Accommodations.Find(x => x.ID == r.AccommodationID)) != null)
                    {
                        r.SetAccommodation(acc);
                        AccommodationUnit au = null;
                        if ((au = r.GetAccommodation().Units.Find(x => x.ID == r.AccommodationUnitID)) != null)
                        {
                            r.SetAccommodationUnit(au);
                        }
                    }
                }
            }

            ReservationsViewModel viewModel = new ReservationsViewModel()
            {
                ReservationSearch = new ReservationSearch(),
                ReservationSort = new ReservationSort(),
                Reservations = userReservations
            };


            return View(viewModel);
        }

        [HttpPost]
        public ActionResult SearchSortReservations()
        {
            ReservationsViewModel viewModel = new ReservationsViewModel()
            {
                ReservationSearch = new ReservationSearch()
                {
                    TouristUsername = Request["username"],
                    Status = (ReservationStatus)Enum.Parse(typeof(ReservationStatus), Request["status"].ToUpper()),
                    ArrangementName = Request["arrangement"],
                    AccommodationName = Request["accommodation"],
                    AccommodationUnitName = Request["accommodation_unit"],
                },
                ReservationSort = new ReservationSort()
                {
                    Option = (ReservationSortOption)Enum.Parse(typeof(ReservationSortOption), Request["reservation_sort"].ToUpper())
                },
                Reservations = ReservationsSerializer.Read().FindAll(x => x.TouristID == ((Tourist)Session["user"]).Username),
            };

            if (Request["search"] != null)
            {
                viewModel.Reservations = SearchReservations(viewModel.Reservations, viewModel.ReservationSearch);
            }
            else
            {
                viewModel.Reservations = SortReservations(viewModel.Reservations, viewModel.ReservationSort);
            }

            List<Arrangement> arrangements = ArrangementsSerializer.Read();

            foreach (var r in viewModel.Reservations)
            {
                r.SetTourist((Tourist)Session["user"]);
                Arrangement a = null;
                if ((a = arrangements.Find(x => x.ID == r.ArrangementID)) != null)
                {
                    r.SetArrangement(a);
                    Accommodation acc = null;
                    if ((acc = a._Accommodations.Find(x => x.ID == r.AccommodationID)) != null)
                    {
                        r.SetAccommodation(acc);
                        AccommodationUnit au = null;
                        if ((au = r.GetAccommodation().Units.Find(x => x.ID == r.AccommodationUnitID)) != null)
                        {
                            r.SetAccommodationUnit(au);
                        }
                    }
                }
            }

            return View("Reservations", viewModel);
        }

        public List<Reservation> SearchReservations(List<Reservation> reservations, ReservationSearch search)
        {
            if (search.TouristUsername != "")
            {
                reservations = reservations.FindAll(x =>
                    x.TouristID.ToUpper().Contains(search.TouristUsername.ToUpper()));
            }
            reservations = reservations.FindAll(x => x._ReservationStatus == search.Status);
            if (search.ArrangementName != "")
            {
                reservations = reservations.FindAll(x =>
                    ArrangementsSerializer.Read(x.ArrangementID).
                    Name.ToUpper().Contains(search.ArrangementName.ToUpper()));
            }
            if (search.AccommodationName != "")
            {
                reservations = reservations.FindAll(x =>
                    ArrangementsSerializer.Read(x.ArrangementID).
                    _Accommodations.Find(y => y.ID == x.AccommodationID).
                    Name.ToUpper().Contains(search.AccommodationName.ToUpper()));
            }

            if (search.AccommodationUnitName != "")
            {
                reservations = reservations.FindAll(x =>
                    ArrangementsSerializer.Read(x.ArrangementID).
                    _Accommodations.Find(y => y.ID == x.AccommodationID).
                    Units.Find(z => z.ID == x.AccommodationUnitID).
                    Name.ToUpper().Contains(search.AccommodationUnitName.ToUpper()));
            }

            return reservations;
        }

        public List<Reservation> SortReservations(List<Reservation> reservations, ReservationSort sort)
        {
            if (reservations.Count > 0)
            {
                switch (sort.Option)
                {
                    case ReservationSortOption.TOURIST_A:
                        reservations = reservations.OrderBy(x => x.TouristID).ToList();
                        break;
                    case ReservationSortOption.TOURIST_D:
                        reservations = reservations.OrderByDescending(x => x.TouristID).ToList();
                        break;
                    case ReservationSortOption.STATUS_A:
                        reservations = reservations.OrderBy(x => x._ReservationStatus).ToList();
                        break;
                    case ReservationSortOption.STATUS_D:
                        reservations = reservations.OrderByDescending(x => x._ReservationStatus).ToList();
                        break;
                    case ReservationSortOption.ARRANGEMENT_A:
                        reservations = reservations.OrderBy(x => ArrangementsSerializer.Read(x.ArrangementID).Name).ToList();
                        break;
                    case ReservationSortOption.ARRANGEMENT_D:
                        reservations = reservations.OrderByDescending(x => ArrangementsSerializer.Read(x.ArrangementID).Name).ToList();
                        break;
                    case ReservationSortOption.ACCOMMODATION_A:
                        reservations = reservations.OrderBy(x => ArrangementsSerializer.Read(x.ArrangementID)._Accommodations.Find(y => y.ID == x.AccommodationID).Name).ToList();
                        break;
                    case ReservationSortOption.ACCOMMODATION_D:
                        reservations = reservations.OrderByDescending(x => ArrangementsSerializer.Read(x.ArrangementID)._Accommodations.Find(y => y.ID == x.AccommodationID).Name).ToList();
                        break;
                    case ReservationSortOption.ACCOMMODATION_UNIT_A:
                        reservations = reservations.OrderBy(x => ArrangementsSerializer.Read(x.ArrangementID)._Accommodations.Find(y => y.ID == x.AccommodationID).Units.Find(z => z.ID == x.AccommodationUnitID)).ToList();
                        break;
                    case ReservationSortOption.ACCOMMODATION_UNIT_D:
                        reservations = reservations.OrderByDescending(x => ArrangementsSerializer.Read(x.ArrangementID)._Accommodations.Find(y => y.ID == x.AccommodationID).Units.Find(z => z.ID == x.AccommodationUnitID)).ToList();
                        break;
                    default:
                        break;
                }
            }
            return reservations;
        }

        [HttpPost]
        public ActionResult CancelReservation()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }
            // read reservation from database and set status to INACTIVE
            Reservation r = ReservationsSerializer.Read(Request["reservation_id"]);
            r._ReservationStatus = ReservationStatus.INACTIVE;

            // increase number of free units
            Accommodation acc = ArrangementsSerializer.ReadAccommodation(r.AccommodationID);
            acc.NumberOfFreeUnits++;

            // read accommodation unit from database
            AccommodationUnit au = acc.Units.Find(x => x.ID == r.AccommodationUnitID);

            // set accommodation unit free
            au.Free = true;


            // increase number of cancelings for tourist
            Tourist t = (Tourist)Session["user"];
            if (t != null)
            {
                t.NumOfCancelings++;
                Session["user"] = t;
            }

            // save changes to database
            ReservationsSerializer.Update(r);
            TouristsSerializer.Update(t, t.Username);
            ArrangementsSerializer.UpdateAccommodation(acc);


            ViewBag.popup = "Reservation successfully canceled!";

            ReservationsViewModel viewModel = new ReservationsViewModel()
            {
                ReservationSearch = new ReservationSearch(),
                ReservationSort = new ReservationSort(),
                Reservations = ReservationsSerializer.Read().FindAll(x => x.TouristID == ((Tourist)Session["user"]).Username)
            };

            List<Arrangement> arrangements = ArrangementsSerializer.Read();

            foreach (var reser in viewModel.Reservations)
            {
                reser.SetTourist((Tourist)Session["user"]);
                Arrangement a = null;
                if ((a = arrangements.Find(x => x.ID == reser.ArrangementID)) != null)
                {
                    reser.SetArrangement(a);
                    Accommodation acco = null;
                    if ((acco = a._Accommodations.Find(y => y.ID == reser.AccommodationID)) != null)
                    {
                        reser.SetAccommodation(acco);
                        AccommodationUnit aun = null;
                        if ((aun = reser.GetAccommodation().Units.Find(z => z.ID == reser.AccommodationUnitID)) != null)
                        {
                            reser.SetAccommodationUnit(aun);
                        }
                    }
                }
            }

            return View("Reservations", viewModel);
        }

        [HttpPost]
        public ActionResult MakeReservation()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UnregisteredUser");
            }
            else
            {
                switch (((User)Session["user"])._Role)
                {
                    case Role.ADMINISTRATOR:
                        return RedirectToAction("Home", "Admin");
                    case Role.MANAGER:
                        return RedirectToAction("Home", "Manager");
                    case Role.TOURIST:
                        break;
                }
            }

            // read accommodation and deacrease number of free units
            Accommodation acc = ArrangementsSerializer.ReadAccommodation(Request["accommodation_id"]);
            acc.NumberOfFreeUnits--;
            AccommodationUnit unit = acc.Units.Find(x => x.ID == Request["accommodation_unit_id"]);
            unit.Free = false;

            // make new reservation for tourist
            Reservation reservation = new Reservation()
            {
                ArrangementID = Request["arrangement_id"],
                AccommodationID = Request["accommodation_id"],
                AccommodationUnitID = Request["accommodation_unit_id"],
                _ReservationStatus = ReservationStatus.ACTIVE,
                TouristID = ((Tourist)Session["user"]).Username
            };

            // read current user(tourist) from database
            Tourist curr = TouristsSerializer.Read(reservation.TouristID);

            // add reservation to tourist reservations
            curr.ReservationsIDs.Add(reservation.ID);

            // read managers from database
            List<Manager> managers = ManagersSerializer.Read();

            // read manager which is owner of arrangement and add reservation id to his list
            Manager m = managers.Find(x => x.ArrangementsIDs.Contains(Request["arrangement_id"]));
            m.ReservationsIDs.Add(reservation.ID);

            // save everything that is changed to database
            ArrangementsSerializer.UpdateAccommodation(acc);
            ReservationsSerializer.Create(reservation);
            TouristsSerializer.Update(curr, curr.Username);
            ManagersSerializer.Update(m, m.Username);

            ViewBag.popup = "Reservation has been made successfully!";

            ReservationsViewModel viewModel = new ReservationsViewModel()
            {
                ReservationSearch = new ReservationSearch(),
                ReservationSort = new ReservationSort(),
                Reservations = ReservationsSerializer.Read().FindAll(x => x.TouristID == curr.Username)
            };

            List<Arrangement> arrangements = ArrangementsSerializer.Read();

            foreach (var reser in viewModel.Reservations)
            {
                reser.SetTourist((Tourist)Session["user"]);
                Arrangement arr = null;
                if ((arr = arrangements.Find(x => x.ID == reser.ArrangementID)) != null)
                {
                    reser.SetArrangement(arr);
                    Accommodation acco = null;
                    if ((acco = arr._Accommodations.Find(y => y.ID == reser.AccommodationID)) != null)
                    {
                        reser.SetAccommodation(acco);
                        AccommodationUnit aun = null;
                        if ((aun = reser.GetAccommodation().Units.Find(z => z.ID == reser.AccommodationUnitID)) != null)
                        {
                            reser.SetAccommodationUnit(aun);
                        }
                    }
                }
            }

            return View("Reservations", viewModel);
        }
        #endregion
    }
}