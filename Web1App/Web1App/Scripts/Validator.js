﻿class Validator {
    static EmptyInput(input) {
        if (input.length == 0) {
            return true;
        }
        return false;
    }
    static ValidateUsername(input) {
        if (this.EmptyInput(input)) {
            return "Empty input."
        }
        var usernameRegex = /^[a-zA-Z0-9]+$/;
        if (!input.match(usernameRegex)) {
            return "Invalid input.";
        }
        return "";
    }
    static ValidatePassword(input) {
        if (this.EmptyInput(input)) {
            return "Empty input."
        }
        if (input.length < 8) {
            return "Password requires at least 8 characters."
        }
        return "";
    }
    static ValidateDate(input) {
        if (this.EmptyInput(input)) {
            return "Empty input."
        }

        let dateformat = /^(0?[1-9]|[1-2][0-9]|3[01])[\/](0?[1-9]|1[0-2])[\/]\d{4}$/;
        if (input.match(dateformat)) {
            let operator = input.split('/');
 
            let datepart = [];
            if (operator.length > 1) {
                datepart = input.split('/');
            }
            let day = parseInt(datepart[0]);
            let month = parseInt(datepart[1]);
            let year = parseInt(datepart[2]);
    
            let ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            if (month == 1 || month > 2) {
                if (day > ListofDays[month - 1]) {    
                    return 'Invalid date format.';
                }
            } else if (month == 2) {
                let leapYear = false;
                if ((!(year % 4) && year % 100) || !(year % 400)) {
                    leapYear = true;
                }
                if ((leapYear == false) && (day >= 29)) {
                    return 'Invalid date format.';
                } else
                    if ((leapYear == true) && (day > 29)) {
                        return 'Invalid date format.';
                    }
            }
        }else{
            return 'Invalid date format.';
        }
        return '';  
    }
    static ValidateEmail(input) {
        if (this.EmptyInput(input)) {
            return "Empty input."
        }
        var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!(input.match(mailformat))) {
            return "Invalid email.";
        }
        return "";
    }
    static ValidateNameOrSurname(input) {
        if (this.EmptyInput(input)) {
            return "Empty input."
        }
        let regExp = /^[A-Za-z\s]+$/;
        if (!input.match(regExp)) {
            return "Invalid input.";
        }
        return "";
    }
}