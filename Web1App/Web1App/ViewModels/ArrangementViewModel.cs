﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web1App.Models;
using Web1App.ViewModelComponents;

namespace Web1App.ViewModels
{
    public class ArrangementViewModel
    {
        public ArrangementViewModel()
        {

        }

        public Arrangement Arrangement { get; set; }

        public AccommodationSearch AccommodationSearch { get; set; }

        public AccommodationSort AccommodationSort { get; set; }
    }
}