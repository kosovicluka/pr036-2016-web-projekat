﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web1App.Models;
using Web1App.ViewModelComponents;

namespace Web1App.ViewModels
{
    public class ArrangementsVM
    {
        public string Title { get; set; }
        public ArrangementsSort Sort { get; set; }
        public ArrangementsSearch Search { get; set; }
        public List<Arrangement> Arrangements { get; set; }
    }
}