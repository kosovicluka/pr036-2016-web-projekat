﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web1App.Models;

namespace Web1App.ViewModels
{
    public class AccommodationUnitViewModel
    {
        public AccommodationUnitViewModel()
        {

        }

        public string ArrangementId { get; set; }

        public string AccommodationId { get; set; }

        public AccommodationUnit AccommodationUnit { get; set; }
    }
}