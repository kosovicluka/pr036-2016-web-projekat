﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web1App.Models;
using Web1App.ViewModelComponents;

namespace Web1App.ViewModels
{
    public class HomeArrangementsViewModel
    {
        public HomeArrangementsViewModel()
        {
            ArrangementsSearch = new ArrangementsSearch();
            Arrangements = new List<Arrangement>();
            ArrangementsSort = new ArrangementsSort();
        }

        public HomeArrangementsViewModel(string title, ArrangementsSearch aSearch, List<Arrangement> arrs, ArrangementsSort aSort)
        {
            Title = title;
            ArrangementsSearch = aSearch;
            Arrangements = arrs;
            ArrangementsSort = aSort;
        }

        public string Title { get; set; }

        public ArrangementsSearch ArrangementsSearch { get; set; }

        public List<Arrangement> Arrangements { get; set; }

        public ArrangementsSort ArrangementsSort { get; set; }
    }
}