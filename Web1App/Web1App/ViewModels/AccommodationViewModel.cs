﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web1App.Models;
using Web1App.ViewModelComponents;

namespace Web1App.ViewModels
{
    public class AccommodationViewModel
    {
        public AccommodationViewModel()
        {

        }

        public string ArrangementId { get; set; }
        public Accommodation Accommodation { get; set; }
        public AccommodationUnitsSearch AccommodationUnitsSearch { get; set; }
        public AccommodationUnitsSort AccommodationUnitsSort { get; set; }
    }
}