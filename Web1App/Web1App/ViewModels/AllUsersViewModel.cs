﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web1App.Models;
using Web1App.ViewModelComponents;

namespace Web1App.ViewModels
{
    public class AllUsersViewModel
    {
        public AllUsersViewModel()
        {

        }
        public UsersSearch Search { get; set; }

        public UsersSort Sort { get; set; }

        public List<User> Users { get; set; }
    }
}