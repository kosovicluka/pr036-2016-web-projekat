﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web1App.Models;
using Web1App.ViewModelComponents;

namespace Web1App.ViewModels
{
    public class ReservationsViewModel
    {
        public ReservationsViewModel()
        {

        }

        public ReservationSearch ReservationSearch { get; set; }
        public ReservationSort ReservationSort { get; set; }
        public List<Reservation> Reservations { get; set; }

    }
}