﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web1App.Models;

namespace Web1App.ViewModels
{
    public class UserViewModel
    {
        public UserViewModel()
        {

        }

        public User User { get; set; }

        public List<SelectListItem> BindGender()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "Male",
                Value = Gender.MALE.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Female",
                Value = Gender.FEMALE.ToString()
            });
            items.Add(new SelectListItem
            {
                Text = "Other",
                Value = Gender.OTHER.ToString()
            });
            SelectListItem item = items.Find(x => x.Value == User._Gender.ToString());
            item.Selected = true;

            return items;
        }
    }
}